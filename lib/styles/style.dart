import 'package:flutter/material.dart';

const largeText = 24.0;
const buttonText = 18.0;
const mediumText = 16.0;
const smallText = 14.0;
const thinText = 10.0;

const String myFont = 'Poppins';

const Color primaryColor = Color(0xFF36C494);
const Color accentGreen = Color(0xFFEEFCF7);
const Color backgroundColor = Color(0xFFF1F2F4);

const Color TextColorDark = Color(0xFF201E2A);
const Color TextColor1 = Color(0xFFA6A5AA);
const Color TextColor2 = Color(0xFFBDB7C2);
const Color TextColorLight = Colors.white;

const Color yellowColor = Color(0xFFFFC400);
const Color blueIcon = Color(0xFF02AFFF);
const Color purpleIcon = Color(0xFF7C4DFF);
const Color redIcon = Color(0xFFF53B5F);

const AppBarTextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: largeText,
    fontWeight: FontWeight.w700,
    color: TextColorDark);

const HeadTextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: largeText,
    fontWeight: FontWeight.w800,
    color: TextColorDark);

const Display1TextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: buttonText,
    fontWeight: FontWeight.w700,
    color: TextColorDark);

const Display2TextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: buttonText,
    fontWeight: FontWeight.w500,
    color: TextColor1);

const Body1TextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: mediumText,
    fontWeight: FontWeight.w700,
    color: TextColorDark);

const Body2TextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: mediumText,
    fontWeight: FontWeight.w500,
    color: TextColorDark);

const Body3TextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: mediumText,
    fontWeight: FontWeight.w500,
    color: TextColor1);

const CaptionTextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: smallText,
    fontWeight: FontWeight.w500,
    color: TextColor1);

const ButtonTextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: buttonText,
    fontWeight: FontWeight.w700,
    color: TextColorLight);
