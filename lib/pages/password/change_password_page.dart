import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:koperasi_jasa/pages/auth/login_page.dart';
import 'package:koperasi_jasa/styles/style.dart' as style;

class ChangePasswordPage extends StatefulWidget {
  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  bool _secureText = true;

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Ganti Password'),
        ),
        body: Container(
          padding: const EdgeInsets.all(16),
          width: double.infinity,
          child: Card(
            color: Colors.white,
            elevation: 0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
              Radius.circular(16),
            )),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 16.0, top: 10),
                    child: Text(
                      'Masukkan Password Baru',
                      style: style.Display2TextStyle,
                    ),
                  ),
                  Form(
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          decoration: InputDecoration(
                              filled: true,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(18),
                                  gapPadding: 0),
                              labelText: 'Password',
                              hintText: 'Masukkan password',
                              suffixIcon: IconButton(
                                  onPressed: showHide,
                                  icon: Icon(_secureText
                                      ? FlutterIcons.eye_off_fea
                                      : FlutterIcons.eye_fea))),
                          obscureText: _secureText,
                          keyboardType: TextInputType.visiblePassword,
                        ),
                        SizedBox(height: 8),
                        TextFormField(
                          decoration: InputDecoration(
                              filled: true,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(18),
                                  gapPadding: 0),
                              labelText: 'Ulangi Password',
                              hintText: 'Masukkan ulang password',
                              suffixIcon: IconButton(
                                  onPressed: showHide,
                                  icon: Icon(_secureText
                                      ? FlutterIcons.eye_off_fea
                                      : FlutterIcons.eye_fea))),
                          obscureText: _secureText,
                          keyboardType: TextInputType.visiblePassword,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 24),
                  Container(
                    width: double.infinity,
                    height: 48,
                    child: FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16)),
                      onPressed: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginPage()));
                      },
                      child: Text(
                        'Simpan',
                        style: style.ButtonTextStyle,
                      ),
                      color: style.primaryColor,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
