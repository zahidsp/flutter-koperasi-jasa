import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../styles/style.dart' as style;
import 'verification_home_page.dart';

class IdentityNextPage extends StatefulWidget {
  @override
  _IdentityNextPageState createState() => _IdentityNextPageState();
}

class _IdentityNextPageState extends State<IdentityNextPage> {
  String noKtp,
      pendTerakhir,
      kantor,
      jabatan,
      statusDiri,
      jmlTanggungan,
      namaWali,
      noWali,
      namaSaudara,
      noSaudara;
  String namaLengkap, jenisKelamin, tempatLahir, tglLahir, noHp, noTlp, alamat;

  final _key = new GlobalKey<FormState>();
  bool loading = false;

  Future getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      namaLengkap = preferences.getString("namaLengkap");
      jenisKelamin = preferences.getString("jenisKelamin");
      tempatLahir = preferences.getString("tempatLahir");
      tglLahir = preferences.getString("tglLahir");
      noTlp = preferences.getString("noTlp");
      noHp = preferences.getString("noHp");
      alamat = preferences.getString("alamat");
    });
  }

  Future savePref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("noKtp", noKtp);
    preferences.setString("pendTerakhir", pendTerakhir);
    preferences.setString("kantor", kantor);
    preferences.setString("jabatan", jabatan);
    preferences.setString("statusDiri", statusDiri);
    preferences.setString("jmlTanggungan", jmlTanggungan);
    preferences.setString("namaWali", namaWali);
    preferences.setString("noWali", noWali);
    preferences.setString("namaSaudara", namaSaudara);
    preferences.setString("noSudara", noSaudara);
    preferences.setBool("fillData", true);
  }

  _checkForm() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      savePref();
      // print('lengkap');

      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => VerificationHomePage()));
    }
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Data Lainnya'),
        ),
        body: ListView(
          children: <Widget>[
            SizedBox(height: 8),
            Card(
              color: Colors.white,
              elevation: 0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                Radius.circular(16),
              )),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Form(
                      key: _key,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          SizedBox(height: 16),
                          TextFormField(
                              keyboardType: TextInputType.number,
                              textCapitalization: TextCapitalization.words,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(18),
                                    gapPadding: 0),
                                labelText: 'Nomor Ktp',
                                hintText: 'Masukkan Nomor Ktp',
                              ),
                              onSaved: (e) => noKtp = e,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return 'wajib diisi';
                                } else {
                                  return null;
                                }
                              }),
                          SizedBox(height: 16),
                          TextFormField(
                              textCapitalization: TextCapitalization.words,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(18),
                                      gapPadding: 0),
                                  labelText: 'Pendidikan Terakhir',
                                  hintText: 'Masukkan Pendidikan Terakhir',
                                  helperText: 'Contoh: S1'),
                              onSaved: (e) => pendTerakhir = e,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return 'wajib diisi';
                                } else {
                                  return null;
                                }
                              }),
                          SizedBox(height: 16),
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: TextFormField(
                                    textCapitalization:
                                        TextCapitalization.words,
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(18),
                                            gapPadding: 0),
                                        labelText: 'Asal Kantor',
                                        hintText: 'Masukkan Kantor',
                                        helperText:
                                            'Contoh: KPTI-Artha Property'),
                                    onSaved: (e) => kantor = e,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return 'wajib diisi';
                                      } else {
                                        return null;
                                      }
                                    }),
                              ),
                              SizedBox(width: 8),
                              Expanded(
                                child: TextFormField(
                                    textCapitalization:
                                        TextCapitalization.words,
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(18),
                                            gapPadding: 0),
                                        labelText: 'Jabatan',
                                        hintText: 'Masukkan Jabatan',
                                        helperText: 'Contoh: Finance'),
                                    onSaved: (e) => jabatan = e,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return 'wajib diisi';
                                      } else {
                                        return null;
                                      }
                                    }),
                              )
                            ],
                          ),
                          SizedBox(height: 16),
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: TextFormField(
                                    textCapitalization:
                                        TextCapitalization.words,
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(18),
                                            gapPadding: 0),
                                        labelText: 'Status',
                                        hintText: 'Masukkan statusDiri',
                                        helperText: 'Contoh: Belum Kawin'),
                                    onSaved: (e) => statusDiri = e,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return 'wajib diisi';
                                      } else {
                                        return null;
                                      }
                                    }),
                              ),
                              SizedBox(width: 8),
                              Expanded(
                                child: TextFormField(
                                    textCapitalization:
                                        TextCapitalization.words,
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(18),
                                            gapPadding: 0),
                                        labelText: 'Jml Tanggungan',
                                        hintText: 'Jml Tanggungan',
                                        helperText: 'Contoh: 2'),
                                    keyboardType: TextInputType.number,
                                    onSaved: (e) => jmlTanggungan = e,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return 'wajib diisi';
                                      } else {
                                        return null;
                                      }
                                    }),
                              )
                            ],
                          ),
                          SizedBox(height: 16),
                          TextFormField(
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(18),
                                    gapPadding: 0),
                                labelText: 'Nama Suami/Istri/Orang tua',
                                hintText: 'Nama Suami/Istri/Orang tua',
                              ),
                              onSaved: (e) => namaWali = e,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return 'wajib diisi';
                                } else {
                                  return null;
                                }
                              }),
                          SizedBox(height: 16),
                          TextFormField(
                              textCapitalization: TextCapitalization.words,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(18),
                                    gapPadding: 0),
                                labelText: 'No. HP Suami/Istri/Orang tua',
                                hintText: 'No. HP Suami/Istri/Orang tua',
                              ),
                              keyboardType: TextInputType.number,
                              onSaved: (e) => noWali = e,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return 'wajib diisi';
                                } else {
                                  return null;
                                }
                              }),
                          SizedBox(height: 16),
                          TextFormField(
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(18),
                                      gapPadding: 0),
                                  labelText: 'Nama Saudara Tidak Serumah',
                                  hintText: 'Nama Saudara Tidak Serumah',
                                  helperText: 'Untuk keperluan darurat'),
                              onSaved: (e) => namaSaudara = e,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return 'wajib diisi';
                                } else {
                                  return null;
                                }
                              }),
                          SizedBox(height: 16),
                          TextFormField(
                              textCapitalization: TextCapitalization.words,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(18),
                                    gapPadding: 0),
                                labelText: 'No. HP Saudara Tidak Serumah',
                                hintText: 'No. HP Saudara Tidak Serumah',
                              ),
                              keyboardType: TextInputType.number,
                              onSaved: (e) => noSaudara = e,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return 'wajib diisi';
                                } else {
                                  return null;
                                }
                              }),
                        ],
                      ),
                    ),
                    SizedBox(height: 16),
                    Container(
                      width: double.infinity,
                      height: 48,
                      child: FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16)),
                        onPressed: () {
                          _checkForm();
                        },
                        child: Text(
                          'Simpan',
                          style: style.ButtonTextStyle,
                        ),
                        color: style.primaryColor,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
