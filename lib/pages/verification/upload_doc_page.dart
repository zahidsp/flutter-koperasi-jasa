import 'dart:io';

import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:shared_preferences/shared_preferences.dart';
import 'verification_home_page.dart';

import '../../services/api.dart';
import '../../styles/style.dart' as style;

class UploadDocPage extends StatefulWidget {
  @override
  _UploadDocPageState createState() => _UploadDocPageState();
}

class _UploadDocPageState extends State<UploadDocPage> {
  File _imageKtp, _imageKk, _imagePay;
  bool loading, uploadDoc;

  Future getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      uploadDoc = preferences.getBool("uploadDoc");
      loading = false;
    });
  }

  Widget _notifFill() {
    if (uploadDoc) {
      return Container(
        margin: const EdgeInsets.only(bottom: 4),
        width: double.infinity,
        height: 32,
        color: style.accentGreen,
        child: Center(
          child: Text(
            'Anda sudah mengunggah dokumen.;',
            style: style.Body3TextStyle.copyWith(color: style.primaryColor),
          ),
        ),
      );
    }

    return Container();
  }

  Future savePref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool("uploadDoc", true);
  }

  Future pickImage(String image, ImageSource source) async {
    try {
      final pickedFile = await ImagePicker().getImage(source: source);

      setState(() {
        if (image == 'ktp') {
          _imageKtp = File(pickedFile.path);
        } else if (image == 'selfie') {
          _imageKk = File(pickedFile.path);
        } else if (image == 'pay') {
          _imagePay = File(pickedFile.path);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  void _chooseImageSource(String imageType) {
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        // height: 146,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 12),
              child: Text('Pilih Sumber',
                  style: style.Display2TextStyle.copyWith(
                      color: style.TextColorDark)),
            ),
            ListTile(
              onTap: () {
                pickImage(imageType, ImageSource.camera);

                Navigator.pop(context);
              },
              leading: CircleAvatar(
                  child:
                      Icon(FlutterIcons.camera_fea, color: style.primaryColor),
                  backgroundColor: style.backgroundColor),
              title: Text('Camera'),
            ),
            ListTile(
              onTap: () {
                pickImage(imageType, ImageSource.gallery);
                Navigator.pop(context);
              },
              leading: CircleAvatar(
                  child:
                      Icon(FlutterIcons.folder_fea, color: style.primaryColor),
                  backgroundColor: style.backgroundColor),
              title: Text('Gallery'),
            )
          ],
        ),
      ),
    );

    showDialog(context: context, child: alertDialog);
  }

  _checkForm() {
    if (_imageKtp != null && _imagePay != null && _imageKk != null) {
      // register();
      savePref();
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => VerificationHomePage()));
    } else {
      print('Mohon Lengkapi Foto');
    }
  }

  Future register() async {
    setState(() {
      loading = true;
    });
    try {
      var uri = Uri.parse(ApiUrl.register);

      var stream =
          new http.ByteStream(DelegatingStream.typed(_imageKtp.openRead()));
      var length = await _imageKtp.length();
      var multiPartFile = new http.MultipartFile("foto_ktp", stream, length,
          filename: path.basename(_imageKtp.path));

      var stream2 =
          new http.ByteStream(DelegatingStream.typed(_imageKk.openRead()));
      var length2 = await _imageKk.length();
      var multiPartFile2 = new http.MultipartFile(
          "foto_selfie_ktp", stream2, length2,
          filename: path.basename(_imageKk.path));

      var stream3 =
          new http.ByteStream(DelegatingStream.typed(_imagePay.openRead()));
      var length3 = await _imagePay.length();
      var multiPartFile3 = new http.MultipartFile(
          "bukti_transfer_pokok", stream3, length3,
          filename: path.basename(_imagePay.path));

      var request = new http.MultipartRequest("POST", uri);
      request.files.add(multiPartFile);
      request.files.add(multiPartFile2);
      request.files.add(multiPartFile3);
      var response = await request.send();

      if (response.statusCode == 200) {
        print('sukses upload');
      } else {
        print('gagal upload');
      }
    } catch (e) {
      print('gagal, +$e');
    }
  }

  @override
  void initState() {
    super.initState();
    loading = true;
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Verifikasi Akun'),
        ),
        body: loading
            ? Center(child: CircularProgressIndicator())
            : ListView(
                children: <Widget>[
                  SizedBox(height: 8),
                  _notifFill(),
                  Card(
                    color: Colors.white,
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    )),
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Text(
                              'Foto KTP',
                              style: style.Body1TextStyle,
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            // color: Colors.white,
                            // padding: const EdgeInsets.only(top: 28, bottom: 12),
                            child: Center(
                                child: _imageKtp == null
                                    ? Container(
                                        padding: const EdgeInsets.all(12),
                                        width: double.infinity,
                                        decoration: BoxDecoration(
                                            color: style.backgroundColor,
                                            borderRadius:
                                                BorderRadius.circular(16)),
                                        child: Center(
                                          child: InkWell(
                                            onTap: () {
                                              _chooseImageSource('ktp');
                                            },
                                            child: Column(
                                              mainAxisSize: MainAxisSize.max,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(
                                                  FlutterIcons.plus_square_fea,
                                                  size: 48.0,
                                                  color: style.TextColor1,
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8),
                                                  child: Text(
                                                    'Tambah foto KTP',
                                                    style: style.Body2TextStyle
                                                        .copyWith(
                                                            color: style
                                                                .TextColor1),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      )
                                    : Center(
                                        child: Stack(
                                          alignment:
                                              AlignmentDirectional.topEnd,
                                          children: <Widget>[
                                            Container(
                                              // height: 120,
                                              width: double.infinity,
                                              child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(16),
                                                  child: Image.file(
                                                    _imageKtp,
                                                    fit: BoxFit.cover,
                                                  )),
                                            ),
                                            Container(
                                              decoration: BoxDecoration(
                                                color: Colors.black12
                                                    .withOpacity(0.3),
                                                borderRadius:
                                                    BorderRadius.circular(16),
                                              ),
                                              height: 40,
                                              width: 120,
                                              child: FlatButton.icon(
                                                onPressed: () {
                                                  _chooseImageSource('ktp');
                                                },
                                                icon: Icon(
                                                  FlutterIcons.edit_fea,
                                                  color: Colors.white,
                                                  size: 10,
                                                ),
                                                label: Text(
                                                  'Edit Gambar',
                                                  style: style.Body2TextStyle
                                                      .copyWith(
                                                          fontSize: 12,
                                                          color: Colors.white),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )),
                          ),
                          SizedBox(height: 8),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Text(
                              'Foto KK',
                              style: style.Body1TextStyle,
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            // color: Colors.white,
                            // padding: const EdgeInsets.only(top: 28, bottom: 12),
                            child: Center(
                                child: _imageKk == null
                                    ? Container(
                                        padding: const EdgeInsets.all(12),
                                        width: double.infinity,
                                        decoration: BoxDecoration(
                                            color: style.backgroundColor,
                                            borderRadius:
                                                BorderRadius.circular(16)),
                                        child: Center(
                                          child: InkWell(
                                            onTap: () {
                                              _chooseImageSource('selfie');
                                            },
                                            child: Column(
                                              mainAxisSize: MainAxisSize.max,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(
                                                  FlutterIcons.plus_square_fea,
                                                  size: 48.0,
                                                  color: style.TextColor1,
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8),
                                                  child: Text('Tambah foto KK',
                                                      style: style
                                                              .Body2TextStyle
                                                          .copyWith(
                                                              color: style
                                                                  .TextColor1)),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      )
                                    : Center(
                                        child: Stack(
                                          alignment:
                                              AlignmentDirectional.topEnd,
                                          children: <Widget>[
                                            Container(
                                              // height: 120,
                                              width: double.infinity,
                                              child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(16),
                                                  child: Image.file(
                                                    _imageKk,
                                                    fit: BoxFit.cover,
                                                  )),
                                            ),
                                            Container(
                                              decoration: BoxDecoration(
                                                color: Colors.black12
                                                    .withOpacity(0.3),
                                                borderRadius:
                                                    BorderRadius.circular(16),
                                              ),
                                              height: 40,
                                              width: 120,
                                              child: FlatButton.icon(
                                                onPressed: () {
                                                  _chooseImageSource('selfie');
                                                },
                                                icon: Icon(
                                                  FlutterIcons.edit_fea,
                                                  color: Colors.white,
                                                  size: 10,
                                                ),
                                                label: Text(
                                                  'Edit Gambar',
                                                  style: style.Body2TextStyle
                                                      .copyWith(
                                                          fontSize: 12,
                                                          color: Colors.white),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )),
                          ),
                          SizedBox(height: 8),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Text(
                              'Bukti Pembayaran Iuran Pokok',
                              style: style.Body1TextStyle,
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            // color: Colors.white,
                            // padding: const EdgeInsets.only(top: 28, bottom: 12),
                            child: Center(
                                child: _imagePay == null
                                    ? Container(
                                        padding: const EdgeInsets.all(12),
                                        width: double.infinity,
                                        decoration: BoxDecoration(
                                            color: style.backgroundColor,
                                            borderRadius:
                                                BorderRadius.circular(16)),
                                        child: Center(
                                          child: InkWell(
                                            onTap: () {
                                              _chooseImageSource('pay');
                                            },
                                            child: Column(
                                              mainAxisSize: MainAxisSize.max,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(
                                                  FlutterIcons.plus_square_fea,
                                                  size: 48.0,
                                                  color: style.TextColor1,
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8),
                                                  child: Text(
                                                      'Upload Bukti Pembayaran',
                                                      style: style
                                                              .Body2TextStyle
                                                          .copyWith(
                                                              color: style
                                                                  .TextColor1)),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      )
                                    : Center(
                                        child: Stack(
                                          alignment:
                                              AlignmentDirectional.topEnd,
                                          children: <Widget>[
                                            Container(
                                              // height: 120,
                                              width: double.infinity,
                                              child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(16),
                                                  child: Image.file(
                                                    _imagePay,
                                                    fit: BoxFit.cover,
                                                  )),
                                            ),
                                            Container(
                                              decoration: BoxDecoration(
                                                color: Colors.black12
                                                    .withOpacity(0.3),
                                                borderRadius:
                                                    BorderRadius.circular(16),
                                              ),
                                              height: 40,
                                              width: 120,
                                              child: FlatButton.icon(
                                                onPressed: () {
                                                  _chooseImageSource('pay');
                                                },
                                                icon: Icon(
                                                  FlutterIcons.edit_fea,
                                                  color: Colors.white,
                                                  size: 10,
                                                ),
                                                label: Text(
                                                  'Edit Gambar',
                                                  style: style.Body2TextStyle
                                                      .copyWith(
                                                          fontSize: 12,
                                                          color: Colors.white),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )),
                          ),
                          SizedBox(height: 16),
                          Container(
                            width: double.infinity,
                            height: 48,
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(16)),
                              onPressed: () {
                                _checkForm();
                              },
                              child: Text(
                                'Daftar Sekarang',
                                style: style.ButtonTextStyle,
                              ),
                              color: style.primaryColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
