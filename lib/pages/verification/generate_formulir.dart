import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:flutter_icons/flutter_icons.dart';

const PdfColor green = PdfColor.fromInt(0xff9ce5d0);
const PdfColor lightGreen = PdfColor.fromInt(0xffcdf1e7);
const PdfColor black = PdfColor.fromInt(0xff000000);

_myPageTheme(PdfPageFormat format) {
  return pw.PageTheme(
    pageFormat: format.applyMargin(
        left: 1.5 * PdfPageFormat.cm,
        top: 1.5 * PdfPageFormat.cm,
        right: 1.5 * PdfPageFormat.cm,
        bottom: 1.5 * PdfPageFormat.cm),
  );
}

generateForm(
    String namaLengkap,
    String jenisKelamin,
    String tempatLahir,
    String tglLahir,
    String alamat,
    String noHp,
    String noTlp,
    String noKtp,
    String pendTerakhir,
    String kantor,
    String jabatan,
    String statusDiri,
    String jmlTanggungan,
    String namaWali,
    String noWali,
    String namaSaudara,
    String noSaudara) {
  Printing.layoutPdf(
    onLayout: (format) async {
      DateTime tanggalLahir = new DateFormat('yyyy-MM-dd').parse(tglLahir);
      final formatTgl = new DateFormat('dd MMMM yyyy');

      String author = "Budi Sugeng";
      // PdfPageFormat format = PdfPageFormat.legal;

      final pw.Document doc =
          pw.Document(author: author, title: 'Form Pendaftaran');

      final pw.PageTheme pageTheme = _myPageTheme(format);

      final PdfImage kopKpti = PdfImage.file(
        doc.document,
        bytes: (await rootBundle.load('assets/images/logoKPTI.png'))
            .buffer
            .asUint8List(),
      );

      final PdfImage iconMap = PdfImage.file(
        doc.document,
        bytes: (await rootBundle.load('assets/images/map.png'))
            .buffer
            .asUint8List(),
      );

      final PdfImage iconPhone = PdfImage.file(
        doc.document,
        bytes: (await rootBundle.load('assets/images/phone.png'))
            .buffer
            .asUint8List(),
      );

      doc.addPage(pw.Page(
          // pageFormat: PdfPageFormat.legal,
          pageTheme: pageTheme,
          build: (pw.Context context) {
            return pw.Column(
                mainAxisSize: pw.MainAxisSize.max,
                crossAxisAlignment: pw.CrossAxisAlignment.start,
                children: <pw.Widget>[
                  pw.Row(
                      mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                      children: <pw.Widget>[
                        pw.Expanded(
                            flex: 1,
                            child: pw.Container(
                                height: 72, child: pw.Image(kopKpti))),
                        pw.Expanded(
                            flex: 1,
                            child: pw.Column(
                                mainAxisAlignment:
                                    pw.MainAxisAlignment.spaceAround,
                                mainAxisSize: pw.MainAxisSize.max,
                                crossAxisAlignment: pw.CrossAxisAlignment.start,
                                children: <pw.Widget>[
                                  // pw.Text('Koperasi Property Today Indonesia',
                                  //     style: pw.TextStyle(
                                  //         fontSize: 16,
                                  //         fontWeight: pw.FontWeight.bold)),
                                  pw.Row(
                                      crossAxisAlignment:
                                          pw.CrossAxisAlignment.start,
                                      children: <pw.Widget>[
                                        pw.Container(
                                            padding: const pw.EdgeInsets.only(
                                                top: 4),
                                            height: 14,
                                            child: pw.Image(iconMap)),
                                        pw.SizedBox(width: 8),
                                        pw.Text(
                                            'Ruko Trimurti Square No. 1-4 Jl.Kaliurang Km.10,\nNgalalangan, Sandonoharjo, Ngaglik,\nSleman Yogyakarta 55581',
                                            style: pw.TextStyle(fontSize: 10)),
                                      ]),
                                  pw.SizedBox(height: 8),
                                  pw.Row(
                                      crossAxisAlignment:
                                          pw.CrossAxisAlignment.center,
                                      children: <pw.Widget>[
                                        pw.Container(
                                            height: 10,
                                            child: pw.Image(iconPhone)),
                                        pw.SizedBox(width: 8),
                                        pw.Text('(0274) 2101817',
                                            style: pw.TextStyle(fontSize: 10)),
                                      ]),
                                ]))
                      ]),
                  pw.Container(
                    width: double.infinity,
                    color: black,
                    height: 3,
                    margin: const pw.EdgeInsets.only(top: 4, bottom: 8),
                  ),
                  pw.Center(
                      child: pw.Text(
                    'Formulir Pendaftaran Menjadi Anggota',
                    style: pw.TextStyle(
                        fontSize: 16,
                        fontWeight: pw.FontWeight.bold,
                        decoration: pw.TextDecoration.underline),
                  )),
                  pw.Padding(
                    padding: const pw.EdgeInsets.only(top: 12, bottom: 4),
                    child: pw.Text(
                      'Yang bertanda tangan dibawah ini,',
                    ),
                  ),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(
                        child: pw.Text('1. '),
                      ),
                    ),
                    pw.Expanded(
                      flex: 10,
                      child: pw.Text('Nama Lengkap '),
                    ),
                    pw.Expanded(
                      flex: 15,
                      child: pw.Text(': ' + '$namaLengkap'),
                    ),
                    pw.Expanded(
                      flex: 14,
                      child: pw.Text('L/P: $jenisKelamin'),
                    ),
                  ])),
                  pw.SizedBox(height: 4),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(
                        child: pw.Text('2. '),
                      ),
                    ),
                    pw.Expanded(
                      flex: 10,
                      child: pw.Text('Tempat, Tgl Lahir'),
                    ),
                    pw.Expanded(
                      flex: 29,
                      child: pw.Text(': ' +
                          '$tempatLahir, ${formatTgl.format(tanggalLahir)}'),
                    ),
                  ])),
                  pw.SizedBox(height: 4),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(
                        child: pw.Text('3. '),
                      ),
                    ),
                    pw.Expanded(
                      flex: 10,
                      child: pw.Text('Alamat Lengkap'),
                    ),
                    pw.Expanded(
                      flex: 29,
                      child: pw.Text(': ' + '$alamat'),
                    ),
                  ])),
                  pw.SizedBox(height: 4),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(
                        child: pw.Text('4. '),
                      ),
                    ),
                    pw.Expanded(
                      flex: 10,
                      child: pw.Text('Telepon'),
                    ),
                    pw.Expanded(
                        flex: 15,
                        child: noTlp != null
                            ? pw.Text(': Rumah : ' + ' $noTlp')
                            : pw.Text(': Rumah : ............')),
                    pw.Expanded(
                      flex: 14,
                      child: pw.Text('HP: ' + '$noHp'),
                    ),
                  ])),
                  pw.SizedBox(height: 4),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(
                        child: pw.Text('5. '),
                      ),
                    ),
                    pw.Expanded(
                      flex: 10,
                      child: pw.Text('Nomor KTP'),
                    ),
                    pw.Expanded(
                      flex: 29,
                      child: pw.Text(': ' + '$noKtp'),
                    ),
                  ])),
                  pw.SizedBox(height: 4),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(
                        child: pw.Text('6. '),
                      ),
                    ),
                    pw.Expanded(
                      flex: 10,
                      child: pw.Text('Pendidikan Terakhir '),
                    ),
                    pw.Expanded(
                      flex: 29,
                      child: pw.Text(': ' + '$pendTerakhir'),
                    ),
                  ])),
                  pw.SizedBox(height: 4),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(
                        child: pw.Text('7. '),
                      ),
                    ),
                    pw.Expanded(
                      flex: 10,
                      child: pw.Text('Asal Kantor'),
                    ),
                    pw.Expanded(
                      flex: 15,
                      child: pw.Text(': ' + '$kantor'),
                    ),
                    pw.Expanded(
                      flex: 14,
                      child: pw.Text('Jabatan: ' + '$jabatan'),
                    ),
                  ])),
                  pw.SizedBox(height: 4),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(
                        child: pw.Text('8. '),
                      ),
                    ),
                    pw.Expanded(
                      flex: 10,
                      child: pw.Text('Status Keluarga'),
                    ),
                    pw.Expanded(
                      flex: 15,
                      child: pw.Text(': ' + '$statusDiri'),
                    ),
                    pw.Expanded(
                      flex: 14,
                      child: pw.Text('Jml. Tanggungan: ' + '$jmlTanggungan'),
                    ),
                  ])),
                  pw.SizedBox(height: 4),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(
                        child: pw.Text('9. '),
                      ),
                    ),
                    pw.Expanded(
                      flex: 10,
                      child: pw.Text('Suami/Istri/Orang tua'),
                    ),
                    pw.Expanded(
                      flex: 15,
                      child: pw.Text(': ' + '$namaWali'),
                    ),
                    pw.Expanded(
                      flex: 14,
                      child: pw.Text('HP: ' + '$noWali'),
                    ),
                  ])),
                  pw.SizedBox(height: 4),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(
                        child: pw.Text('10. '),
                      ),
                    ),
                    pw.Expanded(
                      flex: 10,
                      child: pw.Text('Saudara Tdk Serumah'),
                    ),
                    pw.Expanded(
                      flex: 15,
                      child: pw.Text(': ' + '$namaSaudara'),
                    ),
                    pw.Expanded(
                      flex: 14,
                      child: pw.Text('HP: ' + '$noSaudara'),
                    ),
                  ])),
                  pw.SizedBox(height: 12),
                  pw.Paragraph(
                      text: 'Saya mengajukan permohonan untuk menjadi Anggota Koperasi KPTI.' +
                          '\nSaya Bersedia mentaati Anggaran Dasar, Anggaran Rumah Tangga, Pola kebijakan Pengurus serta peraturan lain yang berlaku pada Koperasi KPTI dengan penuh tanggung jawab'),
                  pw.Padding(
                    padding: const pw.EdgeInsets.only(top: 0, bottom: 4),
                    child: pw.Text(
                      'Bersama Formulir Permohonan ini, saya sertakan :',
                    ),
                  ),
                  pw.Text('1. Satu lembar Fotokopi KTP.'),
                  pw.SizedBox(height: 4),
                  pw.Text(
                      '2. Dua lembar foto ukuran 3X4 dan ukuran 2X3 (ukuran KTP).'),
                  pw.SizedBox(height: 4),
                  pw.Text(
                      '3. Penyetoran Pertama anggota baru yang terdiri dari :'),
                  pw.SizedBox(height: 4),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(),
                    ),
                    pw.Expanded(
                      flex: 8,
                      child: pw.Text('a. Uang Administrasi'),
                    ),
                    pw.Expanded(
                      flex: 20,
                      child: pw.Text(': ' + 'Rp 20.000'),
                    ),
                  ])),
                  pw.SizedBox(height: 4),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(),
                    ),
                    pw.Expanded(
                      flex: 8,
                      child: pw.Text('b. Simpanan Pokok'),
                    ),
                    pw.Expanded(
                      flex: 20,
                      child: pw.Text(
                          ': ' + 'Rp ......................................'),
                    ),
                  ])),
                  pw.SizedBox(height: 4),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(),
                    ),
                    pw.Expanded(
                      flex: 8,
                      child: pw.Text('c. Simpanan Wajib'),
                    ),
                    pw.Expanded(
                      flex: 20,
                      child: pw.Text(': ' + 'Rp 1.000.000 (setiap bulan)'),
                    ),
                  ])),
                  pw.SizedBox(height: 4),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(),
                    ),
                    pw.Expanded(
                      flex: 4,
                      child: pw.Container(),
                    ),
                    pw.Expanded(
                      flex: 4,
                      child: pw.Text('Jumlah'),
                    ),
                    pw.Expanded(
                      flex: 20,
                      child: pw.Text(
                          ': ' + 'Rp ......................................'),
                    ),
                  ])),
                  pw.SizedBox(height: 4),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(),
                    ),
                    pw.Expanded(
                      flex: 4,
                      child: pw.Container(),
                    ),
                    pw.Expanded(
                      flex: 4,
                      child: pw.Text('Terbilang'),
                    ),
                    pw.Expanded(
                      flex: 20,
                      child: pw.Text(': ' +
                          ' ...........................................'),
                    ),
                  ])),
                  pw.SizedBox(height: 12),
                  pw.Paragraph(
                      text:
                          'Demikian permohonan ini saya buat dengan sebenar-benarnya, atas perhatiannya saya ucapkan terima kasih.'),
                  pw.Align(
                      alignment: pw.Alignment.centerRight,
                      child: pw.Text('Sleman, ....... ................ 2020',
                          textAlign: pw.TextAlign.center)),
                  pw.SizedBox(height: 4),
                  pw.Center(
                    child: pw.Text('Mengetahui,'),
                  ),
                  pw.SizedBox(height: 8),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      child: pw.Column(children: <pw.Widget>[
                        pw.Text('Bendahara/Petugas'),
                        pw.SizedBox(height: 60),
                        pw.Text('(............................)')
                      ]),
                    ),
                    pw.Expanded(
                      child: pw.Column(children: <pw.Widget>[
                        pw.Text('Suami/Istri/Orang Tua'),
                        pw.SizedBox(height: 60),
                        pw.Text('(............................)')
                      ]),
                    ),
                    pw.Expanded(
                      child: pw.Column(children: <pw.Widget>[
                        pw.Text('Pemohon'),
                        pw.SizedBox(height: 60),
                        pw.Text('( $namaLengkap )')
                      ]),
                    ),
                  ])),
                ]);
          }));
      return doc.save();
    },
  );
}
