import 'dart:io';

import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:shared_preferences/shared_preferences.dart';

import '../../services/api.dart';
import '../../styles/style.dart' as style;

class FullfillIdentityPage extends StatefulWidget {
  @override
  _FullfillIdentityPageState createState() => _FullfillIdentityPageState();
}

class _FullfillIdentityPageState extends State<FullfillIdentityPage> {
  File _imageKtp, _imageSelfie, _imagePay;
  String username, email, password, namaLengkap, namaIbu, noKtp, noHp, alamat;
  final _key = new GlobalKey<FormState>();
  bool loading = false;

  Future getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      username = preferences.getString("username");
      email = preferences.getString("email");
      password = preferences.getString("password");
    });
  }

  Future pickImage(String image, ImageSource source) async {
    try {
      final pickedFile = await ImagePicker().getImage(source: source);

      setState(() {
        if (image == 'ktp') {
          _imageKtp = File(pickedFile.path);
        } else if (image == 'selfie') {
          _imageSelfie = File(pickedFile.path);
        } else if (image == 'pay') {
          _imagePay = File(pickedFile.path);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  void _chooseImageSource(String imageType) {
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        // height: 146,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 12),
              child: Text('Pilih Sumber',
                  style: style.Display2TextStyle.copyWith(
                      color: style.TextColorDark)),
            ),
            ListTile(
              onTap: () {
                pickImage(imageType, ImageSource.camera);

                Navigator.pop(context);
              },
              leading: CircleAvatar(
                  child:
                      Icon(FlutterIcons.camera_fea, color: style.primaryColor),
                  backgroundColor: style.backgroundColor),
              title: Text('Camera'),
            ),
            ListTile(
              onTap: () {
                pickImage(imageType, ImageSource.gallery);
                Navigator.pop(context);
              },
              leading: CircleAvatar(
                  child:
                      Icon(FlutterIcons.folder_fea, color: style.primaryColor),
                  backgroundColor: style.backgroundColor),
              title: Text('Gallery'),
            )
          ],
        ),
      ),
    );

    showDialog(context: context, child: alertDialog);
  }

  _checkForm() {
    final form = _key.currentState;
    if (form.validate()) {
      if (_imageKtp != null && _imagePay != null && _imageSelfie != null) {
        form.save();
        // print('lengkap');
        register();

        //  Navigator.pushReplacement(
        //     context,
        //     MaterialPageRoute(
        //         builder: (context) => VerificationHomePage(
        //               verif: '0',
        //             )));
      } else {
        print('Mohon Lengkapi Foto');
      }
    }
  }

  Future register() async {
    setState(() {
      loading = true;
    });
    try {
      var uri = Uri.parse(ApiUrl.register);

      var stream =
          new http.ByteStream(DelegatingStream.typed(_imageKtp.openRead()));
      var length = await _imageKtp.length();
      var multiPartFile = new http.MultipartFile("foto_ktp", stream, length,
          filename: path.basename(_imageKtp.path));

      var stream2 =
          new http.ByteStream(DelegatingStream.typed(_imageSelfie.openRead()));
      var length2 = await _imageSelfie.length();
      var multiPartFile2 = new http.MultipartFile(
          "foto_selfie_ktp", stream2, length2,
          filename: path.basename(_imageSelfie.path));

      var stream3 =
          new http.ByteStream(DelegatingStream.typed(_imagePay.openRead()));
      var length3 = await _imagePay.length();
      var multiPartFile3 = new http.MultipartFile(
          "bukti_transfer_pokok", stream3, length3,
          filename: path.basename(_imagePay.path));

      var request = new http.MultipartRequest("POST", uri);

      request.fields['username'] = username;
      request.fields['password'] = password;
      request.fields['email'] = email;
      request.fields['nama_lengkap'] = namaLengkap;
      request.fields['nama_ktp'] = noKtp;
      request.fields['nama_ibu'] = namaIbu;
      request.fields['no_hp'] = noHp;
      request.fields['alamat'] = alamat;
      request.files.add(multiPartFile);
      request.files.add(multiPartFile2);
      request.files.add(multiPartFile3);
      var response = await request.send();

      if (response.statusCode == 200) {
        print('sukses upload');
      } else {
        print('gagal upload');
      }
    } catch (e) {
      print('gagal, +$e');
    }
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Verifikasi Akun'),
        ),
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: <Widget>[
            Card(
              color: Colors.white,
              elevation: 0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                Radius.circular(16),
              )),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Text(
                        'Data Diri',
                        style: style.Body1TextStyle,
                      ),
                    ),
                    Form(
                      key: _key,
                      child: Column(
                        children: <Widget>[
                          SizedBox(height: 8),
                          TextFormField(
                              textCapitalization: TextCapitalization.words,
                              decoration: InputDecoration(
                                filled: true,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(18),
                                    gapPadding: 0),
                                labelText: 'Nama Lengkap',
                                hintText: 'Masukkan nama sesuai KTP',
                              ),
                              onSaved: (e) => namaLengkap = e,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return 'Nama wajib diisi';
                                } else {
                                  return null;
                                }
                              }),
                          SizedBox(height: 8),
                          TextFormField(
                              textCapitalization: TextCapitalization.words,
                              decoration: InputDecoration(
                                filled: true,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(18),
                                    gapPadding: 0),
                                labelText: 'Nama Ibu',
                                hintText: 'Masukkan nama ibu',
                              ),
                              onSaved: (e) => namaIbu = e,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return 'Nama Ibu wajib diisi';
                                } else {
                                  return null;
                                }
                              }),
                          SizedBox(height: 8),
                          TextFormField(
                              textCapitalization: TextCapitalization.words,
                              decoration: InputDecoration(
                                filled: true,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(18),
                                    gapPadding: 0),
                                labelText: 'Nomor KTP',
                                hintText: 'Masukkan nomor KTP',
                              ),
                              keyboardType: TextInputType.number,
                              onSaved: (e) => noKtp = e,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return 'No.KTP wajib diisi';
                                } else {
                                  return null;
                                }
                              }),
                          SizedBox(height: 8),
                          TextFormField(
                              textCapitalization: TextCapitalization.words,
                              decoration: InputDecoration(
                                filled: true,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(18),
                                    gapPadding: 0),
                                labelText: 'Nomor HP',
                                hintText: 'Masukkan nomor HP',
                              ),
                              keyboardType: TextInputType.number,
                              onSaved: (e) => noHp = e,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return 'No. HP wajib diisi';
                                } else {
                                  return null;
                                }
                              }),
                          SizedBox(height: 8),
                          TextFormField(
                              textCapitalization: TextCapitalization.words,
                              decoration: InputDecoration(
                                filled: true,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(18),
                                    gapPadding: 0),
                                labelText: 'Alamat',
                                hintText: 'Masukkan alamat',
                              ),
                              onSaved: (e) => alamat = e,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return 'Alamat wajib diisi';
                                } else {
                                  return null;
                                }
                              }),
                          SizedBox(height: 8),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Text(
                        'Foto KTP',
                        style: style.Body1TextStyle,
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      // color: Colors.white,
                      // padding: const EdgeInsets.only(top: 28, bottom: 12),
                      child: Center(
                          child: _imageKtp == null
                              ? Container(
                                  padding: const EdgeInsets.all(12),
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      color: style.backgroundColor,
                                      borderRadius: BorderRadius.circular(16)),
                                  child: Center(
                                    child: InkWell(
                                      onTap: () {
                                        _chooseImageSource('ktp');
                                      },
                                      child: Column(
                                        mainAxisSize: MainAxisSize.max,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            FlutterIcons.plus_square_fea,
                                            size: 48.0,
                                            color: style.TextColor1,
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 8),
                                            child: Text(
                                              'Tambah foto KTP',
                                              style:
                                                  style.Body2TextStyle.copyWith(
                                                      color: style.TextColor1),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              : Center(
                                  child: Stack(
                                    alignment: AlignmentDirectional.topEnd,
                                    children: <Widget>[
                                      Container(
                                        // height: 120,
                                        width: double.infinity,
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(16),
                                            child: Image.file(
                                              _imageKtp,
                                              fit: BoxFit.cover,
                                            )),
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                          color:
                                              Colors.black12.withOpacity(0.3),
                                          borderRadius:
                                              BorderRadius.circular(16),
                                        ),
                                        height: 40,
                                        width: 120,
                                        child: FlatButton.icon(
                                          onPressed: () {
                                            _chooseImageSource('ktp');
                                          },
                                          icon: Icon(
                                            FlutterIcons.edit_fea,
                                            color: Colors.white,
                                            size: 10,
                                          ),
                                          label: Text(
                                            'Edit Gambar',
                                            style:
                                                style.Body2TextStyle.copyWith(
                                                    fontSize: 12,
                                                    color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )),
                    ),
                    SizedBox(height: 8),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Text(
                        'Foto Selfie dengan KTP',
                        style: style.Body1TextStyle,
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      // color: Colors.white,
                      // padding: const EdgeInsets.only(top: 28, bottom: 12),
                      child: Center(
                          child: _imageSelfie == null
                              ? Container(
                                  padding: const EdgeInsets.all(12),
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      color: style.backgroundColor,
                                      borderRadius: BorderRadius.circular(16)),
                                  child: Center(
                                    child: InkWell(
                                      onTap: () {
                                        _chooseImageSource('selfie');
                                      },
                                      child: Column(
                                        mainAxisSize: MainAxisSize.max,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            FlutterIcons.plus_square_fea,
                                            size: 48.0,
                                            color: style.TextColor1,
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 8),
                                            child: Text('Tambah foto Selfie',
                                                style: style.Body2TextStyle
                                                    .copyWith(
                                                        color:
                                                            style.TextColor1)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              : Center(
                                  child: Stack(
                                    alignment: AlignmentDirectional.topEnd,
                                    children: <Widget>[
                                      Container(
                                        // height: 120,
                                        width: double.infinity,
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(16),
                                            child: Image.file(
                                              _imageSelfie,
                                              fit: BoxFit.cover,
                                            )),
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                          color:
                                              Colors.black12.withOpacity(0.3),
                                          borderRadius:
                                              BorderRadius.circular(16),
                                        ),
                                        height: 40,
                                        width: 120,
                                        child: FlatButton.icon(
                                          onPressed: () {
                                            _chooseImageSource('selfie');
                                          },
                                          icon: Icon(
                                            FlutterIcons.edit_fea,
                                            color: Colors.white,
                                            size: 10,
                                          ),
                                          label: Text(
                                            'Edit Gambar',
                                            style:
                                                style.Body2TextStyle.copyWith(
                                                    fontSize: 12,
                                                    color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )),
                    ),
                    SizedBox(height: 8),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Text(
                        'Bukti Pembayaran Iuran Pokok',
                        style: style.Body1TextStyle,
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      // color: Colors.white,
                      // padding: const EdgeInsets.only(top: 28, bottom: 12),
                      child: Center(
                          child: _imagePay == null
                              ? Container(
                                  padding: const EdgeInsets.all(12),
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      color: style.backgroundColor,
                                      borderRadius: BorderRadius.circular(16)),
                                  child: Center(
                                    child: InkWell(
                                      onTap: () {
                                        _chooseImageSource('pay');
                                      },
                                      child: Column(
                                        mainAxisSize: MainAxisSize.max,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            FlutterIcons.plus_square_fea,
                                            size: 48.0,
                                            color: style.TextColor1,
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 8),
                                            child: Text(
                                                'Upload Bukti Pembayaran',
                                                style: style.Body2TextStyle
                                                    .copyWith(
                                                        color:
                                                            style.TextColor1)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              : Center(
                                  child: Stack(
                                    alignment: AlignmentDirectional.topEnd,
                                    children: <Widget>[
                                      Container(
                                        // height: 120,
                                        width: double.infinity,
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(16),
                                            child: Image.file(
                                              _imagePay,
                                              fit: BoxFit.cover,
                                            )),
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                          color:
                                              Colors.black12.withOpacity(0.3),
                                          borderRadius:
                                              BorderRadius.circular(16),
                                        ),
                                        height: 40,
                                        width: 120,
                                        child: FlatButton.icon(
                                          onPressed: () {
                                            _chooseImageSource('pay');
                                          },
                                          icon: Icon(
                                            FlutterIcons.edit_fea,
                                            color: Colors.white,
                                            size: 10,
                                          ),
                                          label: Text(
                                            'Edit Gambar',
                                            style:
                                                style.Body2TextStyle.copyWith(
                                                    fontSize: 12,
                                                    color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )),
                    ),
                    SizedBox(height: 16),
                    Container(
                      width: double.infinity,
                      height: 48,
                      child: FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16)),
                        onPressed: () {
                          _checkForm();
                        },
                        child: Text(
                          'Daftar Sekarang',
                          style: style.ButtonTextStyle,
                        ),
                        color: style.primaryColor,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
