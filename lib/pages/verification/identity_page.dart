import 'dart:io';

import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datepicker_single/flutter_datepicker_single.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:koperasi_jasa/pages/verification/identity_next_page.dart';
import 'package:path/path.dart' as path;
import 'package:shared_preferences/shared_preferences.dart';

import '../../services/api.dart';
import '../../styles/style.dart' as style;
import '../../components/radio_group.dart';

class IdentityPage extends StatefulWidget {
  @override
  _IdentityPageState createState() => _IdentityPageState();
}

class _IdentityPageState extends State<IdentityPage> {
  String namaLengkap,
      jenisKelamin = "Laki-laki",
      tempatLahir,
      tglLahir,
      noHp,
      noTlp,
      alamat;
  DateTime tanggalLahir = DateTime(2010, 12, 31);
  final formatTgl = new DateFormat('dd MMM yyyy');
  final _tglNow = '${DateFormat.yMd("en_US").format(DateTime.now().toLocal())}';
  final _key = new GlobalKey<FormState>();
  bool loading, fillData;

  int _rgProgramming = 1;

  final List<RadioGroup> _programmingList = [
    RadioGroup(index: 1, text: "Laki-laki"),
    RadioGroup(index: 2, text: "Perempuan"),
  ];

  Widget _buildRadioButton() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: _programmingList
          .map((programming) => Row(
                children: <Widget>[
                  Radio(
                    activeColor: style.primaryColor,
                    value: programming.index,
                    groupValue: _rgProgramming,
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    onChanged: (value) {
                      setState(() {
                        _rgProgramming = value;
                        jenisKelamin = programming.text;
                      });
                    },
                  ),
                  Text(programming.text),
                ],
              ))
          .toList(),
    );
  }

  Future<DateTime> selectDate(BuildContext context) async {
    return await showDatePicker(
      context: context,
      initialDate: tanggalLahir,
      initialDatePickerMode: DatePickerMode.year,
      firstDate: DateTime(1950, 1, 1),
      lastDate: DateTime(2015, 12, 31),
    );
  }

  Future getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      fillData = preferences.getBool("fillData");
      loading = false;
    });
  }

  Future savePref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("namaLengkap", namaLengkap);
    preferences.setString("jenisKelamin", jenisKelamin);
    preferences.setString("tempatLahir", tempatLahir);
    preferences.setString("tglLahir", tanggalLahir.toString());
    preferences.setString("noTlp", noTlp);
    preferences.setString("noHp", noHp);
    preferences.setString("alamat", alamat);
  }

  Widget _notifFill() {
    if (fillData) {
      return Container(
        margin: const EdgeInsets.only(bottom: 4),
        width: double.infinity,
        height: 32,
        color: style.accentGreen,
        child: Center(
          child: Text(
            'Anda sudah mengisi data diri',
            style: style.Body3TextStyle.copyWith(color: style.primaryColor),
          ),
        ),
      );
    }

    return Container();
  }

  _checkForm() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      savePref();
      // print('lengkap');

      Navigator.push(
          context, MaterialPageRoute(builder: (context) => IdentityNextPage()));
    }
  }

  @override
  void initState() {
    super.initState();
    loading = true;
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Data Diri'),
        ),
        body: loading
            ? Center(child: CircularProgressIndicator())
            : ListView(
                children: <Widget>[
                  SizedBox(height: 8),
                  _notifFill(),
                  Card(
                    color: Colors.white,
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    )),
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Form(
                            key: _key,
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                SizedBox(height: 16),
                                TextFormField(
                                    textCapitalization:
                                        TextCapitalization.words,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(18),
                                          gapPadding: 0),
                                      labelText: 'Nama Lengkap',
                                      hintText: 'Masukkan nama sesuai KTP',
                                    ),
                                    onSaved: (e) => namaLengkap = e,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return 'Nama wajib diisi';
                                      } else {
                                        return null;
                                      }
                                    }),
                                SizedBox(height: 16),
                                Container(
                                  padding:
                                      const EdgeInsets.only(top: 8, left: 8),
                                  height: 68,
                                  decoration: BoxDecoration(
                                      border:
                                          Border.all(color: style.TextColor1),
                                      borderRadius: BorderRadius.circular(16)),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        'Jenis Kelamin',
                                        style: TextStyle(fontSize: 16),
                                      ),
                                      _buildRadioButton(),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 16),
                                Row(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Expanded(
                                      child: TextFormField(
                                          textCapitalization:
                                              TextCapitalization.words,
                                          decoration: InputDecoration(
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(18),
                                                gapPadding: 0),
                                            labelText: 'Tempat Lahir',
                                            hintText: 'Masukkan Tempat Lahir',
                                          ),
                                          onSaved: (e) => tempatLahir = e,
                                          validator: (e) {
                                            if (e.isEmpty) {
                                              return 'Tempat Lahir wajib diisi';
                                            } else {
                                              return null;
                                            }
                                          }),
                                    ),
                                    SizedBox(width: 8),
                                    Expanded(
                                      child: Container(
                                        padding: const EdgeInsets.only(
                                            top: 8, left: 8),
                                        height: 56,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: style.TextColor1),
                                            borderRadius:
                                                BorderRadius.circular(16)),
                                        child: InkWell(
                                            onTap: () async {
                                              DateTime selected =
                                                  await selectDate(context);

                                              if (selected != null) {
                                                setState(() {
                                                  tanggalLahir = selected;
                                                  tglLahir = formatTgl
                                                      .format(tanggalLahir);
                                                });
                                              }
                                            },
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  'Pilih Tgl Lahir',
                                                  style:
                                                      TextStyle(fontSize: 16),
                                                ),
                                                Text(
                                                    '${formatTgl.format(tanggalLahir)}',
                                                    style: style.Body3TextStyle
                                                        .copyWith(
                                                            color: style
                                                                .primaryColor)),
                                              ],
                                            )),
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: 16),
                                TextFormField(
                                    textCapitalization:
                                        TextCapitalization.words,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(18),
                                          gapPadding: 0),
                                      labelText: 'Nomor HP',
                                      hintText: 'Masukkan nomor HP',
                                    ),
                                    keyboardType: TextInputType.number,
                                    onSaved: (e) => noHp = e,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return 'No. HP wajib diisi';
                                      } else {
                                        return null;
                                      }
                                    }),
                                SizedBox(height: 16),
                                TextFormField(
                                  textCapitalization: TextCapitalization.words,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(18),
                                        gapPadding: 0),
                                    labelText: 'Nomor Telpon',
                                    hintText: 'Masukkan nomor Telpon',
                                  ),
                                  keyboardType: TextInputType.number,
                                  onSaved: (e) => noTlp = e,
                                ),
                                SizedBox(height: 16),
                                TextFormField(
                                    textCapitalization:
                                        TextCapitalization.words,
                                    maxLines: 3,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(18),
                                          gapPadding: 0),
                                      labelText: 'Alamat',
                                      hintText: 'Masukkan alamat',
                                    ),
                                    onSaved: (e) => alamat = e,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return 'Alamat wajib diisi';
                                      } else {
                                        return null;
                                      }
                                    }),
                                SizedBox(height: 16),
                              ],
                            ),
                          ),
                          SizedBox(height: 16),
                          Container(
                            width: double.infinity,
                            height: 48,
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(16)),
                              onPressed: () {
                                _checkForm();
                              },
                              child: Text(
                                'Selanjutnya',
                                style: style.ButtonTextStyle,
                              ),
                              color: style.primaryColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
