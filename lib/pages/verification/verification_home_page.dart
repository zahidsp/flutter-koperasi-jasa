import 'dart:convert';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import '../auth/login_page.dart';
import '../auth/register_page.dart';
import 'identity_next_page.dart';
import 'identity_page.dart';
import '../../services/api.dart';
import '../../styles/style.dart' as style;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'fullfill_identity_page.dart';
import 'upload_doc_page.dart';
import 'generate_formulir.dart';

class VerificationHomePage extends StatefulWidget {
  @override
  _VerificationHomePageState createState() => _VerificationHomePageState();
}

class _VerificationHomePageState extends State<VerificationHomePage> {
  final GlobalKey<RefreshIndicatorState> _refreshKey =
      GlobalKey<RefreshIndicatorState>();
  bool loading=true;
  String username,
      namaLengkap,
      email,
      password,
      verifAkun,
      jenisKelamin,
      tempatLahir,
      tglLahir,
      alamat,
      noHp,
      noTlp,
      noKtp,
      pendTerakhir,
      kantor,
      jabatan,
      statusDiri,
      jmlTanggungan,
      namaWali,
      noWali,
      namaSaudara,
      noSaudara;

  bool fillData, uploadDoc;

  Future getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      username = preferences.getString("username");
      namaLengkap = preferences.getString("namaLengkap");
      jenisKelamin = preferences.getString("jenisKelamin");
      tempatLahir = preferences.getString("tempatLahir");
      tglLahir = preferences.getString("tglLahir");
      alamat = preferences.getString("alamat");
      noHp = preferences.getString("noHp");
      noTlp = preferences.getString("noTlp");
      noKtp = preferences.getString("noKtp");
      namaWali = preferences.getString("namaWali");
      email = preferences.getString("email");
      password = preferences.getString("password");
      verifAkun = preferences.getString("verifAkun");
      fillData = preferences.getBool("fillData");
      uploadDoc = preferences.getBool("uploadDoc");
      pendTerakhir = preferences.getString("pendTerakhir");
      kantor = preferences.getString("kantor");
      jabatan = preferences.getString("jabatan");
      statusDiri = preferences.getString("statusDiri");
      jmlTanggungan = preferences.getString("jmlTanggungan");
      namaWali = preferences.getString("namaWali");
      noWali = preferences.getString("noWali");
      namaSaudara = preferences.getString("namaSaudara");
      noSaudara = preferences.getString("noSudara");
      loading = false;
    });
  }

  Widget _statusVerif(verifAkun) {
    if (verifAkun == '0') {
      return Container(
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
            border: Border.all(color: style.redIcon, width: 1),
            borderRadius: BorderRadius.circular(16)),
        child: Text('Belum Verifikasi',
            style: style.Body2TextStyle.copyWith(
                color: style.redIcon, fontSize: 12)),
      );
    }
    return Text(
      '10231278247',
      style: style.Body3TextStyle.copyWith(
          color: style.primaryColor, fontSize: 18),
    );
  }

  Widget belumVerif() {
    // return DottedBorder(
    //     strokeWidth: 2,
    //     dashPattern: [8, 8],
    //     strokeCap: StrokeCap.round,
    //     color: style.TextColor1,
    //     borderType: BorderType.RRect,
    //     radius: Radius.circular(16),
    //     padding: const EdgeInsets.all(12),
    //     child: Container(
    //       width: double.infinity,
    //       child: Column(
    //         mainAxisSize: MainAxisSize.max,
    //         crossAxisAlignment: CrossAxisAlignment.start,
    //         children: <Widget>[
    //           SizedBox(height: 8),
    //           Text(
    //             'Verifikasi Akun',
    //             style: style.HeadTextStyle,
    //           ),
    //           SizedBox(height: 12),
    //           Text(
    //             'Diperlukan beberapa data untuk melakukan verifikasi :',
    //             style: style.Body3TextStyle,
    //           ),
    //           SizedBox(height: 16),
    //           Text(
    //             '- Data Diri \n- Foto KTP \n- Foto Selfie dengan KTP \n- Bukti transfer iuran pokok',
    //             style: style.Body3TextStyle,
    //           ),
    //           SizedBox(height: 16),
    //           Container(
    //             width: double.infinity,
    //             height: 48,
    //             child: FlatButton(
    //               shape: RoundedRectangleBorder(
    //                   borderRadius: BorderRadius.circular(16)),
    //               onPressed: () {
    //                 Navigator.push(
    //                     context,
    //                     MaterialPageRoute(
    //                         builder: (context) => FullfillIdentityPage()));
    //               },
    //               child: Text(
    //                 'Mulai Verifikasi',
    //                 style: style.ButtonTextStyle,
    //               ),
    //               color: style.primaryColor,
    //             ),
    //           ),
    //         ],
    //       ),
    //     ));
  }

  Widget cardRefresh() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child: DottedBorder(
          strokeWidth: 2,
          dashPattern: [8, 8],
          strokeCap: StrokeCap.round,
          color: style.TextColor1,
          borderType: BorderType.RRect,
          radius: Radius.circular(16),
          padding: const EdgeInsets.all(12),
          child: Container(
            width: double.infinity,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Tips',
                  style: style.HeadTextStyle,
                ),
                Center(
                    child: Icon(
                  FlutterIcons.arrow_down_circle_fea,
                  color: style.TextColor2,
                  size: 54,
                )),
                SizedBox(height: 12),
                Center(
                  child: Text(
                    'Tarik kebawah untuk memuat ulang status verifikasi',
                    style: style.Body3TextStyle,
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          )),
    );
  }

  Widget _cardVerif(verifAkun) {
    if (verifAkun == '1') {
      return DottedBorder(
          strokeWidth: 2,
          dashPattern: [8, 8],
          strokeCap: StrokeCap.round,
          color: style.TextColor1,
          borderType: BorderType.RRect,
          radius: Radius.circular(16),
          padding: const EdgeInsets.all(12),
          child: Container(
            width: double.infinity,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 8),
                Text(
                  'Verifikasi Berhasil',
                  style:
                      style.HeadTextStyle.copyWith(color: style.primaryColor),
                ),
                SizedBox(height: 12),
                Text(
                  'Selamat, Akun Anda berhasil diverifikasi.\nSilahkan login ulang menggunakan username :',
                  style: style.Body3TextStyle,
                ),
                SizedBox(height: 16),
                Text(
                  '$username',
                  style: style.Body1TextStyle,
                ),
                SizedBox(height: 24),
                Container(
                  width: double.infinity,
                  height: 48,
                  child: FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16)),
                    onPressed: () {
                      signOut();
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (context) => LoginPage()),
                          (Route<dynamic> route) => false);
                    },
                    child: Text(
                      'Login Ulang',
                      style: style.ButtonTextStyle,
                    ),
                    color: style.primaryColor,
                  ),
                ),
              ],
            ),
          ));
    }

    return Container(
      width: double.infinity,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Verifikasi Akun',
            style: style.Display1TextStyle,
          ),
          SizedBox(height: 4),
          Text(
            'Lengkapi data berikut untuk melakukan verifikasi:',
            style: style.Body3TextStyle,
          ),
          SizedBox(height: 16),
          Ink(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(16)),
              child: ListTile(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => IdentityPage()));
                },
                title: Text('Data Diri'),
                subtitle: fillData
                    ? Text('Sudah diisi',
                        style: style.Body2TextStyle.copyWith(
                            color: style.primaryColor, fontSize: 12))
                    : Text('Belum diisi',
                        style: style.Body2TextStyle.copyWith(
                            color: style.redIcon, fontSize: 12)),
                trailing: Icon(FlutterIcons.arrow_right_circle_fea,
                    color: style.TextColor1),
              )),
          SizedBox(height: 8),
          Ink(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(16)),
              child: ListTile(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => UploadDocPage()));
                },
                title: Text('Upload Dokumen'),
                subtitle: uploadDoc
                    ? Text('Sudah diisi',
                        style: style.Body2TextStyle.copyWith(
                            color: style.primaryColor, fontSize: 12))
                    : Text('Belum diisi',
                        style: style.Body2TextStyle.copyWith(
                            color: style.redIcon, fontSize: 12)),
                trailing: Icon(FlutterIcons.arrow_right_circle_fea,
                    color: style.TextColor1),
              )),
          SizedBox(height: 8),
        ],
      ),
    );
  }

  Future getVerif() async {
    setState(() {
      loading = true;
    });

    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();

      setState(() {
        preferences.setString("verifAkun", "1");
        getPref();
        loading = false;
      });
    } catch (e) {
      print(e);
    }
  }

  signOut() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.clear();
    });
  }

  @override
  void initState() {
    super.initState();
    loading = true;
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: RefreshIndicator(
            onRefresh: getVerif,
            key: _refreshKey,
            child: loading
                ? Center(child: CircularProgressIndicator())
                : Container(
                    child: ListView(
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.all(16),
                          color: Colors.white,
                          width: double.infinity,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Container(
                                height: 80,
                                width: 80,
                                decoration: BoxDecoration(
                                    color: style.backgroundColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(16))),
                                child: Center(
                                  child: Icon(
                                    FlutterIcons.user_fea,
                                    color: style.TextColor1,
                                    size: 32,
                                  ),
                                ),
                              ),
                              SizedBox(width: 16),
                              Column(
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    'Selamat Datang,',
                                    style: style.CaptionTextStyle,
                                  ),
                                  Text(
                                    '$username',
                                    style: style.Display1TextStyle.copyWith(
                                        fontSize: 22),
                                  ),
                                  SizedBox(height: 4),
                                  _statusVerif(verifAkun)
                                ],
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 8),
                        Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: _cardVerif(verifAkun)),
                        Visibility(
                            // visible: true,
                            visible: uploadDoc && fillData && verifAkun == '0'
                                ? true
                                : false,
                            child: Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 16),
                                width: double.infinity,
                                height: 48,
                                child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(16)),
                                  onPressed: () {
                                    generateForm(
                                        namaLengkap,
                                        jenisKelamin,
                                        tempatLahir,
                                        tglLahir,
                                        alamat,
                                        noHp,
                                        noTlp,
                                        noKtp,
                                        pendTerakhir,
                                        kantor,
                                        jabatan,
                                        statusDiri,
                                        jmlTanggungan,
                                        namaWali,
                                        noWali,
                                        namaSaudara,
                                        noSaudara);
                                  },
                                  child: Text(
                                    'Download Formulir',
                                    style: style.ButtonTextStyle,
                                  ),
                                  color: style.primaryColor,
                                ))),
                        SizedBox(height: 24),
                        Visibility(
                          visible: uploadDoc && fillData && verifAkun == '0'
                              ? true
                              : false,
                          child: cardRefresh(),
                        ),
                        // SizedBox(height: 24),
                        // RaisedButton(
                        //   onPressed: () {
                        //     signOut();
                        //     Navigator.pushAndRemoveUntil(
                        //         context,
                        //         MaterialPageRoute(
                        //             builder: (context) => RegisterPage()),
                        //         (Route<dynamic> route) => false);
                        //   },
                        //   child: Text('logout'),
                        // ),
                      ],
                    ),
                  )),
      ),
    );
  }
}
