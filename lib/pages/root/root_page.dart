import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:koperasi_jasa/pages/home/home_page.dart';
import 'package:koperasi_jasa/pages/payment/new_payment_page.dart';
import 'package:koperasi_jasa/pages/profil/profil_page.dart';
import 'package:koperasi_jasa/styles/style.dart' as style;

class RootPage extends StatefulWidget {
  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  int index = 0;
  List<Widget> listWigdet = [HomePage(), ProfilPage()];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: listWigdet[index],
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => NewPaymentPage()));
          },
          backgroundColor: style.primaryColor,
          child: Icon(FlutterIcons.plus_fea, color: Colors.white),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: BottomAppBar(
          shape: CircularNotchedRectangle(),
          clipBehavior: Clip.antiAlias,
          notchMargin: 5,
          child: BottomNavigationBar(
              onTap: (val) {
                setState(() {
                  index = val;
                });
              },
              currentIndex: index,
              items: [
                BottomNavigationBarItem(
                    icon: Icon(FlutterIcons.home_fea,
                        color:
                            index == 0 ? style.primaryColor : style.TextColor2),
                    title: SizedBox.shrink()),
                BottomNavigationBarItem(
                    icon: Icon(FlutterIcons.user_fea,
                        color:
                            index == 1 ? style.primaryColor : style.TextColor2),
                    title: SizedBox.shrink()),
              ]),
        ),
      ),
    );
  }
}
