import 'package:flutter/material.dart';
import 'package:koperasi_jasa/styles/style.dart' as style;

class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Tentang'),
        ),
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: <Widget>[
            Container(
              width: double.infinity,
              child: Card(
                color: Colors.white,
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                  Radius.circular(16),
                )),
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: 120,
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(16),
                            child: Image.asset(
                              'assets/images/logoKPTI.png',
                              fit: BoxFit.cover,
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 24, bottom: 12),
                        child: Text('Koperasi Jasa Property Today',
                            style: style.HeadTextStyle),
                      ),
                      Text(
                        'Aute minim esse velit ad irure velit. Nisi duis occaecat voluptate velit esse eiusmod labore sint ullamco est sunt ex. Reprehenderit veniam ipsum quis pariatur velit laborum eu enim magna est nostrud in. Cillum officia ipsum enim proident. Veniam anim minim sunt ut esse aute irure voluptate laboris ea laboris qui. Nisi aliquip magna amet id.',
                        style: style.Body2TextStyle,
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
