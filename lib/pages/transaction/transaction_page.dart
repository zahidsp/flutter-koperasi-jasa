import 'package:flutter/material.dart';
import 'package:flutter_datepicker_single/flutter_datepicker_single.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:intl/intl.dart';
import 'package:koperasi_jasa/pages/transaction/detail_transaction_page.dart';
import 'package:koperasi_jasa/styles/style.dart' as style;

class TransactionPage extends StatefulWidget {
  @override
  _TransactionPageState createState() => _TransactionPageState();
}

class _TransactionPageState extends State<TransactionPage> {
  DateTime selectedDate, _tanggalRiwayat;
  int _result;
  var formatTanggal = new DateFormat('yyyy-MM-d');

  Widget _statusVerif(verification) {
    if (verification == '1') {
      return Container(
        padding: const EdgeInsets.all(4),
        width: 80,
        height: 32,
        decoration: BoxDecoration(
            border: Border.all(color: style.primaryColor, width: 2),
            borderRadius: BorderRadius.circular(16)),
        child: Center(
          child: Text('Sukses',
              style: style.Body2TextStyle.copyWith(
                  color: style.primaryColor, fontSize: 14)),
        ),
      );
    }
    return Container(
      padding: const EdgeInsets.all(4),
      width: 80,
      height: 32,
      decoration: BoxDecoration(
          border: Border.all(color: style.yellowColor, width: 2),
          borderRadius: BorderRadius.circular(16)),
      child: Center(
        child: Text('Menunggu',
            style: style.Body2TextStyle.copyWith(
                color: style.yellowColor, fontSize: 14)),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _tanggalRiwayat = DateTime.now();
    _result = _tanggalRiwayat.year;
    selectedDate = _tanggalRiwayat;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Riwayat Iuran'),
        ),
        body: ListView(
          padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
          children: <Widget>[
            Container(
              height: 54,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    'Pilih Tahun',
                    style: style.Body3TextStyle,
                  ),
                  Container(
                    child: InkWell(
                        onTap: () async {
                          DateTime selected = await showYearPicker(
                            context: context,
                            initialDate: selectedDate,
                            firstDate: DateTime(2017, 1, 1),
                            lastDate: DateTime(2050, 12, 31),
                          );

                          if (selected != null) {
                            setState(() {
                              selectedDate = selected;
                              _result = selected.year;
                            });
                          }
                        },
                        child: Row(
                          children: <Widget>[
                            Text('$_result',
                                style: style.Display2TextStyle.copyWith(
                                    color: style.primaryColor)),
                            Icon(
                              FlutterIcons.chevron_down_fea,
                              color: style.primaryColor,
                            )
                          ],
                        )),
                  ),
                ],
              ),
            ),
            Ink(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(16)),
              child: ListTile(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DetailTransactionPage()));
                },
                leading: Container(
                    height: 42,
                    width: 42,
                    decoration: BoxDecoration(
                      border: Border.all(color: style.primaryColor, width: 1.5),
                      borderRadius: BorderRadius.circular(21),
                    ),
                    child: Icon(
                      FlutterIcons.dollar_sign_fea,
                      size: 18,
                      color: style.primaryColor,
                    )),
                title: Text('07 Juli 2020',
                    style: style.Body3TextStyle.copyWith(fontSize: 14)),
                subtitle: Text('Rp 10.000.000', style: style.Body2TextStyle),
                trailing: _statusVerif('0'),
              ),
            ),
            SizedBox(height: 4),
            Ink(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(16)),
              child: ListTile(
                onTap: () {},
                leading: Container(
                    height: 42,
                    width: 42,
                    decoration: BoxDecoration(
                      border: Border.all(color: style.primaryColor, width: 1.5),
                      borderRadius: BorderRadius.circular(21),
                    ),
                    child: Icon(
                      FlutterIcons.dollar_sign_fea,
                      size: 18,
                      color: style.primaryColor,
                    )),
                title: Text('06 Juni 2020',
                    style: style.Body3TextStyle.copyWith(fontSize: 14)),
                subtitle: Text('Rp 10.000.000', style: style.Body2TextStyle),
                trailing: _statusVerif('1'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
