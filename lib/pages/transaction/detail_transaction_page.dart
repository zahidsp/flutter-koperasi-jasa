import 'package:flutter/material.dart';
import 'package:koperasi_jasa/styles/style.dart' as style;

class DetailTransactionPage extends StatefulWidget {
  @override
  _DetailTransactionPageState createState() => _DetailTransactionPageState();
}

class _DetailTransactionPageState extends State<DetailTransactionPage> {
  Widget _statusVerif(verification) {
    if (verification == '1') {
      return Container(
        padding: const EdgeInsets.all(4),
        width: 80,
        height: 32,
        decoration: BoxDecoration(
            border: Border.all(color: style.primaryColor, width: 2),
            borderRadius: BorderRadius.circular(16)),
        child: Center(
          child: Text('Sukses',
              style: style.Body2TextStyle.copyWith(
                  color: style.primaryColor, fontSize: 14)),
        ),
      );
    }
    return Container(
      padding: const EdgeInsets.all(4),
      width: 80,
      height: 32,
      decoration: BoxDecoration(
          border: Border.all(color: style.yellowColor, width: 2),
          borderRadius: BorderRadius.circular(16)),
      child: Center(
        child: Text('Menunggu',
            style: style.Body2TextStyle.copyWith(
                color: style.yellowColor, fontSize: 14)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Detail Tansaksi'),
        ),
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: <Widget>[
            Container(
              width: double.infinity,
              child: Card(
                color: Colors.white,
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                  Radius.circular(16),
                )),
                child: Padding(
                  padding: const EdgeInsets.all(12),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text('07 Juli 2020', style: style.Body3TextStyle),
                          _statusVerif('0')
                        ],
                      ),
                      Padding(
                          padding: const EdgeInsets.only(bottom: 8, top: 24),
                          child: Text(
                            'Pembayaran Iuran',
                            style: style.Body2TextStyle.copyWith(fontSize: 18),
                          )),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text('Iuran Wajib',
                              style: style.Body2TextStyle.copyWith(
                                  color: style.primaryColor)),
                          Text('Rp 10.000.000', style: style.Body3TextStyle),
                        ],
                      ),
                      Padding(
                          padding: const EdgeInsets.only(bottom: 8, top: 24),
                          child: Text(
                            'Bukti Pembayaran',
                            style: style.Body2TextStyle.copyWith(fontSize: 18),
                          )),
                      Container(
                        width: double.infinity,
                        height: 160,
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(16),
                            child: Image.asset(
                              'assets/images/logoKPTI.png',
                              fit: BoxFit.cover,
                            )),
                      ),
                      Padding(
                          padding: const EdgeInsets.only(bottom: 8, top: 24),
                          child: Text(
                            'Keterangan',
                            style: style.Body2TextStyle.copyWith(fontSize: 18),
                          )),
                      TextField(
                        maxLines: 3,
                        enabled: false,
                        decoration: InputDecoration(
                            filled: true,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(18),
                                gapPadding: 0)),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
