import 'package:flutter/services.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';

const PdfColor black = PdfColor.fromInt(0xff000000);
_myPageTheme(PdfPageFormat format) {
  return pw.PageTheme(
    pageFormat: format.applyMargin(
        left: 3 * PdfPageFormat.cm,
        top: 3 * PdfPageFormat.cm,
        right: 2 * PdfPageFormat.cm,
        bottom: 2 * PdfPageFormat.cm),
  );
}

void printShu() {
  Printing.layoutPdf(
    onLayout: (pageFormat) async {
      String author = "Budi Sugeng";
      PdfPageFormat format = PdfPageFormat.a4;

      final pw.Document doc =
          pw.Document(author: author, title: 'Form Pendaftaran');

      final pw.PageTheme pageTheme = _myPageTheme(format);

      final PdfImage kopKpti = PdfImage.file(
        doc.document,
        bytes: (await rootBundle.load('assets/images/logoKPTI.png'))
            .buffer
            .asUint8List(),
      );

      doc.addPage(pw.Page(
          pageTheme: pageTheme,
          build: (pw.Context context) {
            return pw.Column(
                mainAxisSize: pw.MainAxisSize.max,
                crossAxisAlignment: pw.CrossAxisAlignment.start,
                children: <pw.Widget>[
                  pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                        flex: 1,
                        child:
                            pw.Container(height: 53, child: pw.Image(kopKpti))),
                    pw.Expanded(
                        flex: 3,
                        child: pw.Column(
                            mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                            mainAxisSize: pw.MainAxisSize.max,
                            crossAxisAlignment: pw.CrossAxisAlignment.center,
                            children: <pw.Widget>[
                              pw.Text('Koperasi Property Today Indonesia',
                                  style: pw.Theme.of(context)
                                      .defaultTextStyle
                                      .copyWith(
                                          fontWeight: pw.FontWeight.bold)),
                              pw.Text(
                                  'Alamat: Jalan Kaliurang km.13 Gentan, Sleman, DIY. 55678'),
                              pw.Text(
                                  'Telepon (0274) 348938437 Email: admin.kpti@gmail.com'),
                            ]))
                  ]),
                  pw.Container(
                    width: double.infinity,
                    color: black,
                    height: 3,
                    margin: const pw.EdgeInsets.only(top: 4, bottom: 8),
                  ),
                  pw.Center(
                      child: pw.Text(
                    'Sisa Hasil Usaha',
                    style: pw.TextStyle(
                      fontSize: 20,
                      fontWeight: pw.FontWeight.bold,
                    ),
                  )),
                  pw.Padding(
                    padding: const pw.EdgeInsets.only(top: 12, bottom: 12),
                    child: pw.Text(
                      'Perhitungan Laba Rugi',
                    ),
                  ),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(),
                    ),
                    pw.Expanded(
                      flex: 9,
                      child: pw.Text(
                        'Proyek',
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold,
                        ),
                      ),
                    ),
                    pw.Expanded(
                      flex: 30,
                      child: pw.Text(
                        'Rp 3.000.000.000',
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold,
                        ),
                      ),
                    ),
                  ])),
                  pw.SizedBox(height: 8),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(),
                    ),
                    pw.Expanded(
                      flex: 9,
                      child: pw.Text(
                        'Modal',
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold,
                        ),
                      ),
                    ),
                    pw.Expanded(
                      flex: 30,
                      child: pw.Text(
                        'Rp 1.200.000.000',
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold,
                        ),
                      ),
                    ),
                  ])),
                  pw.SizedBox(height: 8),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(),
                    ),
                    pw.Expanded(
                      flex: 9,
                      child: pw.Text(
                        'Laba Kotor',
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold,
                        ),
                      ),
                    ),
                    pw.Expanded(
                      flex: 30,
                      child: pw.Text(
                        'Rp 1.800.000.000',
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold,
                        ),
                      ),
                    ),
                  ])),
                  pw.SizedBox(height: 8),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(),
                    ),
                    pw.Expanded(
                      flex: 9,
                      child: pw.Text(
                        'Biaya Usaha',
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold,
                        ),
                      ),
                    ),
                    pw.Expanded(
                      flex: 30,
                      child: pw.Text(
                        'Rp 200.000.000',
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold,
                        ),
                      ),
                    ),
                  ])),
                  pw.SizedBox(height: 8),
                  pw.Container(
                      child: pw.Row(children: <pw.Widget>[
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(),
                    ),
                    pw.Expanded(
                      flex: 9,
                      child: pw.Text(
                        'Laba Bersih',
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold,
                        ),
                      ),
                    ),
                    pw.Expanded(
                      flex: 30,
                      child: pw.Text(
                        'Rp 1.600.000.000',
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold,
                        ),
                      ),
                    ),
                  ])),
                  pw.SizedBox(height: 8),
                ]);
          }));
      return doc.save();
    },
  );
}
