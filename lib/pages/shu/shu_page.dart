import 'package:flutter/material.dart';
import 'package:flutter_datepicker_single/flutter_datepicker_single.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:intl/intl.dart';
import 'package:koperasi_jasa/pages/shu/generate_shu.dart';
import 'package:koperasi_jasa/styles/style.dart' as style;

class ShuPage extends StatefulWidget {
  @override
  _ShuPageState createState() => _ShuPageState();
}

class _ShuPageState extends State<ShuPage> {
  DateTime selectedDate, _tanggalRiwayat;
  int _result;
  var formatTanggal = new DateFormat('yyyy-MM-d');

  @override
  void initState() {
    super.initState();
    _tanggalRiwayat = DateTime.now();
    _result = _tanggalRiwayat.year;
    selectedDate = _tanggalRiwayat;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('SHU'),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                FlutterIcons.printer_fea,
                color: style.TextColorDark,
              ),
              onPressed: () {
                printShu();
              },
            )
          ],
        ),
        body: ListView(
          children: <Widget>[
            Container(
              height: 54,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    'Pilih Tahun',
                    style: style.Body3TextStyle,
                  ),
                  Container(
                    child: InkWell(
                        onTap: () async {
                          DateTime selected = await showYearPicker(
                            context: context,
                            initialDate: selectedDate,
                            firstDate: DateTime(2017, 1, 1),
                            lastDate: DateTime(2050, 12, 31),
                          );

                          if (selected != null) {
                            setState(() {
                              selectedDate = selected;
                              _result = selected.year;
                            });
                          }
                        },
                        child: Row(
                          children: <Widget>[
                            Text('$_result',
                                style: style.Display2TextStyle.copyWith(
                                    color: style.primaryColor)),
                            Icon(
                              FlutterIcons.chevron_down_fea,
                              color: style.primaryColor,
                            )
                          ],
                        )),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(16),
              width: double.infinity,
              color: Colors.white,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8, top: 4),
                    child: Text(
                      'Perhitungan Laba Rugi',
                      style: style.Body1TextStyle,
                    ),
                  ),
                  SizedBox(height: 4),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Proyek',
                          style: style.CaptionTextStyle.copyWith(
                              color: style.TextColorDark)),
                      Text('Rp 3.000.000.000', style: style.Body3TextStyle),
                    ],
                  ),
                  SizedBox(height: 4),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Modal',
                          style: style.CaptionTextStyle.copyWith(
                              color: style.TextColorDark)),
                      Text('Rp 1.200.000.000', style: style.Body3TextStyle),
                    ],
                  ),
                  SizedBox(height: 4),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Laba Kotor',
                          style: style.CaptionTextStyle.copyWith(
                              color: style.TextColorDark)),
                      Text('Rp 1.800.000.000', style: style.Body3TextStyle),
                    ],
                  ),
                  SizedBox(height: 4),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Biaya Usaha',
                          style: style.CaptionTextStyle.copyWith(
                              color: style.TextColorDark)),
                      Text('Rp 200.000.000', style: style.Body3TextStyle),
                    ],
                  ),
                  SizedBox(height: 4),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Laba Bersih',
                          style: style.CaptionTextStyle.copyWith(
                              color: style.TextColorDark)),
                      Text('Rp 1.600.000.000',
                          style: style.Body3TextStyle.copyWith(
                              color: style.primaryColor)),
                    ],
                  ),
                  SizedBox(height: 4),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
