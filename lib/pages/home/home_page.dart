import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:koperasi_jasa/pages/about/about_page.dart';
import 'package:koperasi_jasa/pages/history/payment_history_page.dart';
import 'package:koperasi_jasa/pages/shu/shu_page.dart';
import 'package:koperasi_jasa/pages/transaction/detail_transaction_page.dart';
import 'package:koperasi_jasa/pages/transaction/transaction_page.dart';
import 'package:koperasi_jasa/styles/style.dart' as style;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Widget _statusVerif(verification) {
    if (verification == '1') {
      return Container(
        padding: const EdgeInsets.all(4),
        width: 80,
        height: 32,
        decoration: BoxDecoration(
            border: Border.all(color: style.primaryColor, width: 2),
            borderRadius: BorderRadius.circular(16)),
        child: Center(
          child: Text('Sukses',
              style: style.Body2TextStyle.copyWith(
                  color: style.primaryColor, fontSize: 14)),
        ),
      );
    }
    return Container(
      padding: const EdgeInsets.all(4),
      width: 80,
      height: 32,
      decoration: BoxDecoration(
          border: Border.all(color: style.yellowColor, width: 2),
          borderRadius: BorderRadius.circular(16)),
      child: Center(
        child: Text('Menunggu',
            style: style.Body2TextStyle.copyWith(
                color: style.yellowColor, fontSize: 14)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
            child: Container(
              padding: const EdgeInsets.all(16),
              color: Colors.white,
              width: double.infinity,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    width: 72,
                    height: 72,
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(16),
                        child: Image.asset(
                          'assets/images/logoKPTI.png',
                          fit: BoxFit.cover,
                        )),
                  ),
                  SizedBox(width: 16),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Selamat Datang,',
                        style: style.CaptionTextStyle,
                      ),
                      Text(
                        'Arief Rahmat',
                        style: style.Display1TextStyle,
                      ),
                      Text(
                        '10231278247',
                        style: style.Display2TextStyle.copyWith(
                            color: style.primaryColor),
                      ),
                    ],
                  )
                ],
              ),
            ),
            preferredSize: Size.fromHeight(96)),
        body: ListView(
          padding:
              const EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 8),
          children: <Widget>[
            Container(
              width: double.infinity,
              child: Card(
                color: Colors.white,
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                  Radius.circular(16),
                )),
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: const EdgeInsets.all(12),
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: style.primaryColor,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(16),
                              topRight: Radius.circular(16))),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Total Iuran',
                                  style: style.Body3TextStyle.copyWith(
                                      color: Colors.white),
                                ),
                                SizedBox(height: 2),
                                Text(
                                  'Rp 30.000.000',
                                  style: style.Body1TextStyle.copyWith(
                                      color: Colors.white, fontSize: 18),
                                )
                              ]),
                          Container(
                            height: 32,
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(16)),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            PaymentHistoryPage()));
                              },
                              child: Text(
                                'Detail',
                                style: style.ButtonTextStyle.copyWith(
                                    color: style.primaryColor),
                              ),
                              color: style.backgroundColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                      height: 24,
                                      width: 24,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            color: style.primaryColor,
                                            width: 1.5),
                                        borderRadius: BorderRadius.circular(12),
                                      ),
                                      child: Icon(
                                        FlutterIcons.dollar_sign_fea,
                                        size: 14,
                                        color: style.primaryColor,
                                      )),
                                  SizedBox(width: 12),
                                  Text(
                                    'Pokok',
                                    style: style.Body3TextStyle,
                                  ),
                                ],
                              ),
                              Text(
                                'Rp 10.000.000',
                                style: style.Body2TextStyle,
                              ),
                            ])),
                    SizedBox(height: 10),
                    Container(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                      height: 24,
                                      width: 24,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            color: style.primaryColor,
                                            width: 1.5),
                                        borderRadius: BorderRadius.circular(12),
                                      ),
                                      child: Icon(
                                        FlutterIcons.dollar_sign_fea,
                                        size: 14,
                                        color: style.primaryColor,
                                      )),
                                  SizedBox(width: 12),
                                  Text(
                                    'Wajib',
                                    style: style.Body3TextStyle,
                                  ),
                                ],
                              ),
                              Text(
                                'Rp 20.000.000',
                                style: style.Body2TextStyle,
                              ),
                            ])),
                    SizedBox(height: 10),
                    Container(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                      height: 24,
                                      width: 24,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            color: style.primaryColor,
                                            width: 1.5),
                                        borderRadius: BorderRadius.circular(12),
                                      ),
                                      child: Icon(
                                        FlutterIcons.dollar_sign_fea,
                                        size: 14,
                                        color: style.primaryColor,
                                      )),
                                  SizedBox(width: 12),
                                  Text(
                                    'Sukarela',
                                    style: style.Body3TextStyle,
                                  ),
                                ],
                              ),
                              Text(
                                'Rp 0',
                                style: style.Body2TextStyle,
                              ),
                            ])),
                    SizedBox(height: 12),
                  ],
                ),
              ),
            ),
            SizedBox(height: 8),
            Text('Menu', style: style.Display1TextStyle),
            Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    child: Container(
                        width: double.infinity,
                        height: 72,
                        child: Card(
                            color: Colors.white,
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                              Radius.circular(16),
                            )),
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            TransactionPage()));
                              },
                              child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Icon(
                                      FlutterIcons.briefcase_fea,
                                      color: style.primaryColor,
                                    ),
                                    Text('Transaksi',
                                        style: style.CaptionTextStyle.copyWith(
                                            color: style.primaryColor))
                                  ]),
                            )))),
                SizedBox(width: 4),
                Expanded(
                    child: Container(
                        width: double.infinity,
                        height: 72,
                        child: Card(
                            color: Colors.white,
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                              Radius.circular(16),
                            )),
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ShuPage()));
                              },
                              child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Icon(
                                      FlutterIcons.book_fea,
                                      color: style.primaryColor,
                                    ),
                                    Text('SHU',
                                        style: style.CaptionTextStyle.copyWith(
                                            color: style.primaryColor))
                                  ]),
                            )))),
                SizedBox(width: 4),
                Expanded(
                    child: Container(
                        width: double.infinity,
                        height: 72,
                        child: Card(
                            color: Colors.white,
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                              Radius.circular(16),
                            )),
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => AboutPage()));
                              },
                              child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Icon(
                                      FlutterIcons.alert_circle_fea,
                                      color: style.primaryColor,
                                    ),
                                    Text('Tentang',
                                        style: style.CaptionTextStyle.copyWith(
                                            color: style.primaryColor))
                                  ]),
                            )))),
              ],
            ),
            SizedBox(height: 8),
            Text('Tagihan', style: style.Display1TextStyle),
            SizedBox(height: 8),
            Ink(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(16)),
              child: ListTile(
                onTap: () {},
                leading: Container(
                    height: 42,
                    width: 42,
                    decoration: BoxDecoration(
                      border: Border.all(color: style.redIcon, width: 1.5),
                      borderRadius: BorderRadius.circular(21),
                    ),
                    child: Icon(
                      FlutterIcons.dollar_sign_fea,
                      size: 18,
                      color: style.redIcon,
                    )),
                title: Text('Iuran Wajib Juni',
                    style: style.Body3TextStyle.copyWith(fontSize: 14)),
                subtitle: Text('Rp 10.000.000', style: style.Body2TextStyle),
              ),
            ),
            SizedBox(height: 8),
            Text('Transaksi Terbaru', style: style.Display1TextStyle),
            SizedBox(height: 8),
            Ink(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(16)),
              child: ListTile(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DetailTransactionPage()));
                },
                leading: Container(
                    height: 42,
                    width: 42,
                    decoration: BoxDecoration(
                      border: Border.all(color: style.primaryColor, width: 1.5),
                      borderRadius: BorderRadius.circular(21),
                    ),
                    child: Icon(
                      FlutterIcons.dollar_sign_fea,
                      size: 18,
                      color: style.primaryColor,
                    )),
                title: Text('07 Juli 2020',
                    style: style.Body3TextStyle.copyWith(fontSize: 14)),
                subtitle: Text('Rp 10.000.000', style: style.Body2TextStyle),
                trailing: _statusVerif('0'),
              ),
            ),
            SizedBox(height: 4),
            Ink(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(16)),
              child: ListTile(
                onTap: () {},
                leading: Container(
                    height: 42,
                    width: 42,
                    decoration: BoxDecoration(
                      border: Border.all(color: style.primaryColor, width: 1.5),
                      borderRadius: BorderRadius.circular(21),
                    ),
                    child: Icon(
                      FlutterIcons.dollar_sign_fea,
                      size: 18,
                      color: style.primaryColor,
                    )),
                title: Text('06 Juni 2020',
                    style: style.Body3TextStyle.copyWith(fontSize: 14)),
                subtitle: Text('Rp 10.000.000', style: style.Body2TextStyle),
                trailing: _statusVerif('1'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
