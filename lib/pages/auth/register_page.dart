import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../styles/style.dart' as style;
import '../verification/verification_home_page.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

enum LoginStatus { notSignIn, signIn }

class _RegisterPageState extends State<RegisterPage> {
  LoginStatus _loginStatus = LoginStatus.notSignIn;

  final _formKey = GlobalKey<FormState>();

  bool _secureText = true, _secureText2 = true;

  String username, email, password, rePassword;
  FlutterToast flutterToast;

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  showHide2() {
    setState(() {
      _secureText2 = !_secureText2;
    });
  }

  _showToastFail() {
    Widget toastWithButton = Container(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16.0),
        color: style.redIcon,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: Text(
              "Password harus sama",
              softWrap: true,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
          IconButton(
            icon: Icon(FlutterIcons.x_circle_fea),
            color: Colors.white,
            onPressed: () {
              flutterToast.removeCustomToast();
            },
          )
        ],
      ),
    );
    flutterToast.showToast(
      child: toastWithButton,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );
  }

  _showToastSuccess() {
    Widget toastWithButton = Container(
      height: 200,
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16.0),
        color: style.accentGreen,
      ),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Center(
                child: CircularProgressIndicator(
              backgroundColor: style.primaryColor,
            )),
            SizedBox(height: 32),
            Text(
              "Registrasi Berhasil, Mohon tunggu...",
              softWrap: true,
              style: style.Body2TextStyle.copyWith(
                color: style.primaryColor,
              ),
            ),
          ],
        ),
      ),
    );
    flutterToast.showToast(
      child: toastWithButton,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );
  }

  _checkForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();

      if (password == rePassword) {
        savePref();
        _showToastSuccess();
        Future.delayed(Duration(seconds: 2), () {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => VerificationHomePage()),
              (Route<dynamic> route) => false);
        });
      } else {
        _showToastFail();
      }
    }
  }

  Future savePref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("username", username);
    preferences.setString("email", email);
    preferences.setString("password", password);
    preferences.setString("isLogin", "success");
    preferences.setString("verifAkun", "0");
    preferences.setBool("fillData", false);
    preferences.setBool("uploadDoc", false);
  }

  @override
  void initState() {
    super.initState();
    flutterToast = FlutterToast(context);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                height: 160,
                margin: const EdgeInsets.only(top: 0),
                child: Center(
                  child: Image.asset(
                    'assets/images/logoKPTI.png',
                    height: 80,
                  ),
                ),
              ),
              Expanded(
                  child: KeyboardAvoider(
                autoScroll: true,
                child: Card(
                  color: Colors.white,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                    Radius.circular(16),
                  )),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 16.0),
                          child: Text(
                            'Registrasi',
                            style: style.HeadTextStyle,
                          ),
                        ),
                        Form(
                          key: _formKey,
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 16),
                              TextFormField(
                                textCapitalization: TextCapitalization.words,
                                decoration: InputDecoration(
                                  filled: true,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(18),
                                      gapPadding: 0),
                                  labelText: 'Username',
                                  hintText: 'Masukkan username',
                                ),
                                onSaved: (e) => username = e,
                                validator: (e) {
                                  if (e.isEmpty) {
                                    return 'Username wajib diisi';
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                              SizedBox(height: 16),
                              TextFormField(
                                keyboardType: TextInputType.emailAddress,
                                decoration: InputDecoration(
                                  filled: true,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(18),
                                      gapPadding: 0,
                                      borderSide:
                                          new BorderSide(color: Colors.red)),
                                  labelText: 'Email',
                                  hintText: 'Masukkan alamat email',
                                ),
                                onSaved: (e) => email = e,
                                validator: (e) {
                                  if (e.isEmpty) {
                                    return 'Email wajib diisi';
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                              SizedBox(height: 16),
                              TextFormField(
                                decoration: InputDecoration(
                                    filled: true,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(18),
                                        gapPadding: 0),
                                    labelText: 'Password',
                                    hintText: 'Masukkan password',
                                    suffixIcon: IconButton(
                                        onPressed: showHide,
                                        icon: Icon(_secureText
                                            ? FlutterIcons.eye_off_fea
                                            : FlutterIcons.eye_fea))),
                                obscureText: _secureText,
                                keyboardType: TextInputType.visiblePassword,
                                onSaved: (e) => password = e,
                                validator: (e) {
                                  if (e.isEmpty) {
                                    return 'Password wajib diisi';
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                              SizedBox(height: 16),
                              TextFormField(
                                decoration: InputDecoration(
                                    filled: true,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(18),
                                        gapPadding: 0),
                                    labelText: 'Ulangi Password',
                                    hintText: 'Masukkan ulang password',
                                    suffixIcon: IconButton(
                                        onPressed: showHide2,
                                        icon: Icon(_secureText2
                                            ? FlutterIcons.eye_off_fea
                                            : FlutterIcons.eye_fea))),
                                obscureText: _secureText2,
                                keyboardType: TextInputType.visiblePassword,
                                onSaved: (e) => rePassword = e,
                                validator: (e) {
                                  if (e.isEmpty) {
                                    return 'Wajib diisi';
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                              SizedBox(height: 16),
                            ],
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: 48,
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(16)),
                            onPressed: () {
                              _checkForm();
                            },
                            child: Text(
                              'Daftar Sekarang',
                              style: style.ButtonTextStyle,
                            ),
                            color: style.primaryColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }
}
