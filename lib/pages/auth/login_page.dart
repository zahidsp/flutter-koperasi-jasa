import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:koperasi_jasa/pages/auth/register_page.dart';
import 'package:koperasi_jasa/pages/root/root_page.dart';
import 'package:koperasi_jasa/services/api.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import './../verification/verification_home_page.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../styles/style.dart' as style;

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

enum LoginStatus { notSignIn, signIn }
enum VerifStatus { notVerif, isVerif }

class _LoginPageState extends State<LoginPage> {
  LoginStatus _loginStatus = LoginStatus.notSignIn;
  VerifStatus _verifStatus = VerifStatus.notVerif;
  FlutterToast flutterToast;
  final _key = new GlobalKey<FormState>();
  String username, password;
  bool _secureText = true, _isLoading = false;
  var isLogin, isVerified;

  _showToastFail(String e) {
    Widget toastWithButton = Container(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16.0),
        color: style.redIcon,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: Text(
              "Login Gagal, $e",
              softWrap: true,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
          IconButton(
            icon: Icon(FlutterIcons.x_circle_fea),
            color: Colors.white,
            onPressed: () {
              flutterToast.removeCustomToast();
            },
          )
        ],
      ),
    );
    flutterToast.showToast(
      child: toastWithButton,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );
  }

  _showToastSuccess() {
    Widget toastWithButton = Container(
      height: 200,
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16.0),
        color: style.accentGreen,
      ),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Center(
                child: CircularProgressIndicator(
              backgroundColor: style.primaryColor,
            )),
            SizedBox(height: 32),
            Text(
              "Login Berhasil, Mohon tunggu...",
              softWrap: true,
              style: style.Body2TextStyle.copyWith(
                color: style.primaryColor,
              ),
            ),
          ],
        ),
      ),
    );
    flutterToast.showToast(
      child: toastWithButton,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );
  }

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  _checkForm() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      login();
    }
  }

  login() async {
    try {
      final response = await http.post(ApiUrl.login,
          body: {"username": username, "password": password});
      final data = jsonDecode(response.body);

      String id = data['id_user'].toString();
      String status = data['status'];
      isVerified = data['is_verifed'].toString();

      if (status == "success") {
        SharedPreferences preferences = await SharedPreferences.getInstance();

        getUser(id);
        // print(isVerified);
        _showToastSuccess();
        Future.delayed(Duration(seconds: 2), () {
          setState(() {
            _loginStatus = LoginStatus.signIn;
            preferences.setString("isLogin", status);
          });
        });
      } else {
        // print(data);
        _showToastFail(status);
        // _failToast();
      }
    } catch (e) {
      _showToastFail(e.toString());
    }
  }

  Future getUser(String id) async {
    try {
      final response = await http.get(ApiUrl.getUser + id);

      final data = jsonDecode(response.body);
      String status = data['status'];
      if (status == 'success') {
        SharedPreferences preferences = await SharedPreferences.getInstance();
        setState(() {
          preferences.setString("id", id);
          preferences.setString("username", data['username']);
          preferences.setString("email", data['email']);
          preferences.setString("namaLengkap", data['nama_lengkap']);
          preferences.setString("noKtp", data['nama_ktp']);
          preferences.setString("namaIbu", data['nama_ibu']);
          preferences.setString("noHp", data['no_hp'].toString());
          preferences.setString("alamat", data['alamat']);
          preferences.setString("fotoKtp", data['foto_ktp']);
          preferences.setString("fotoSelfie", data['foto_selfie_ktp']);
          preferences.setString("verifAkun", data['is_verifed'].toString());
        });
        print('insige getPref' + '${preferences.getString("verifAkun")}');

        // print(preferences.getString('isVerified'));
        // isVerified = preferences.getString('isVerified');
      } else {
        print(data);
      }
    } catch (e) {
      print(e);
    }
  }

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      isLogin = preferences.getString("isLogin");
      isVerified = preferences.getString('verifAkun');

      _loginStatus =
          isLogin == "success" ? LoginStatus.signIn : LoginStatus.notSignIn;
    });
  }

  @override
  void initState() {
    super.initState();
    getPref();
    flutterToast = FlutterToast(context);
  }

  @override
  Widget build(BuildContext context) {
    switch (_loginStatus) {
      case LoginStatus.notSignIn:
        return SafeArea(
          child: Scaffold(
            body: Container(
              padding: const EdgeInsets.all(16),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                      flex: 2,
                      child: Container(
                        margin: const EdgeInsets.only(top: 18),
                        child: Center(
                          child: Image.asset(
                            'assets/images/logoKPTI.png',
                            height: 92,
                          ),
                        ),
                      )),
                  Expanded(
                      flex: 3,
                      child: KeyboardAvoider(
                        autoScroll: true,
                        child: Card(
                          color: Colors.white,
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                            Radius.circular(16),
                          )),
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Text(
                                    'Login',
                                    style: style.HeadTextStyle,
                                  ),
                                ),
                                Form(
                                  key: _key,
                                  child: Column(
                                    children: <Widget>[
                                      SizedBox(height: 16),
                                      TextFormField(
                                          decoration: InputDecoration(
                                            filled: true,
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(18),
                                                gapPadding: 0,
                                                borderSide: new BorderSide(
                                                    color: Colors.red)),
                                            labelText: 'Username',
                                            hintText: 'Masukkan username',
                                          ),
                                          onSaved: (e) => username = e,
                                          validator: (e) {
                                            if (e.isEmpty) {
                                              return 'Username wajib diisi';
                                            } else {
                                              return null;
                                            }
                                          }),
                                      SizedBox(height: 16),
                                      TextFormField(
                                          decoration: InputDecoration(
                                              filled: true,
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(18),
                                                  gapPadding: 0),
                                              labelText: 'Password',
                                              hintText: 'Masukkan password',
                                              suffixIcon: IconButton(
                                                  onPressed: showHide,
                                                  icon: Icon(_secureText
                                                      ? FlutterIcons.eye_off_fea
                                                      : FlutterIcons.eye_fea))),
                                          obscureText: _secureText,
                                          keyboardType:
                                              TextInputType.visiblePassword,
                                          onSaved: (e) => password = e,
                                          validator: (e) {
                                            if (e.isEmpty) {
                                              return 'Password wajib diisi';
                                            } else {
                                              return null;
                                            }
                                          }),
                                      SizedBox(height: 16),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: double.infinity,
                                  height: 48,
                                  child: FlatButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(16)),
                                    onPressed: () {
                                      _checkForm();
                                    },
                                    child: Text(
                                      'Masuk',
                                      style: style.ButtonTextStyle,
                                    ),
                                    color: style.primaryColor,
                                  ),
                                ),
                                Column(
                                  children: <Widget>[
                                    Center(
                                      child: Text('Belum punya akun?',
                                          style: style.Body3TextStyle),
                                    ),
                                    SizedBox(height: 16),
                                    Container(
                                      width: double.infinity,
                                      height: 48,
                                      child: FlatButton(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(16)),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      RegisterPage()));
                                        },
                                        child: Text('Daftar',
                                            style:
                                                style.ButtonTextStyle.copyWith(
                                                    color: style.primaryColor)),
                                        color: style.backgroundColor,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ))
                ],
              ),
            ),
          ),
        );
        break;

      case LoginStatus.signIn:
        // print(isVerified);
        // return RootPage();
        if (isVerified == '1') {
          return RootPage();
        } else {
          return VerificationHomePage();
        }
        break;
    }
  }
}
