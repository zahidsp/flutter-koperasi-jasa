import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:intl/intl.dart';
import 'package:flutter_datepicker_single/flutter_datepicker_single.dart';
import 'package:koperasi_jasa/styles/style.dart' as style;

class PaymentHistoryPage extends StatefulWidget {
  @override
  _PaymentHistoryPageState createState() => _PaymentHistoryPageState();
}

class _PaymentHistoryPageState extends State<PaymentHistoryPage> {
  DateTime selectedDate, _tanggalRiwayat;
  int _result;
  var formatTanggal = new DateFormat('yyyy-MM-d');

  @override
  void initState() {
    super.initState();
    _tanggalRiwayat = DateTime.now();
    _result = _tanggalRiwayat.year;
    selectedDate = _tanggalRiwayat;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Riwayat Iuran'),
        ),
        body: ListView(padding: const EdgeInsets.only(bottom: 16), children: <
            Widget>[
          Container(
            height: 54,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(
                  'Pilih Tahun',
                  style: style.Body3TextStyle,
                ),
                Container(
                  child: InkWell(
                      onTap: () async {
                        DateTime selected = await showYearPicker(
                          context: context,
                          initialDate: selectedDate,
                          firstDate: DateTime(2017, 1, 1),
                          lastDate: DateTime(2050, 12, 31),
                        );

                        if (selected != null) {
                          setState(() {
                            selectedDate = selected;
                            _result = selected.year;
                          });
                        }
                      },
                      child: Row(
                        children: <Widget>[
                          Text('$_result',
                              style: style.Display2TextStyle.copyWith(
                                  color: style.primaryColor)),
                          Icon(
                            FlutterIcons.chevron_down_fea,
                            color: style.primaryColor,
                          )
                        ],
                      )),
                ),
              ],
            ),
          ),
          Container(
              height: 48,
              padding: const EdgeInsets.symmetric(horizontal: 8),
              color: style.primaryColor,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: Center(
                          child: Text(
                        'Bulan',
                        style:
                            style.Body3TextStyle.copyWith(color: Colors.white),
                      ))),
                  Expanded(
                      flex: 2,
                      child: Center(
                          child: Text(
                        'Pokok (Rp)',
                        style:
                            style.Body3TextStyle.copyWith(color: Colors.white),
                      ))),
                  Expanded(
                      flex: 2,
                      child: Center(
                          child: Text(
                        'Wajib (Rp)',
                        style:
                            style.Body3TextStyle.copyWith(color: Colors.white),
                      ))),
                  Expanded(
                      flex: 2,
                      child: Center(
                          child: Text(
                        'Sukarela (Rp)',
                        style:
                            style.Body3TextStyle.copyWith(color: Colors.white),
                      ))),
                ],
              )),
          Container(
              height: 48,
              padding: const EdgeInsets.symmetric(vertical: 4),
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: style.TextColor2))),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: Center(
                          child: Text(
                        '3',
                        style: style.CaptionTextStyle.copyWith(
                            color: style.TextColorDark),
                      ))),
                  Expanded(
                      flex: 2,
                      child: Center(
                          child: Text(
                        'Rp 10.000.000',
                        style: style.CaptionTextStyle,
                      ))),
                  Expanded(
                      flex: 2,
                      child: Center(
                          child: Text(
                        'Rp 0',
                        style: style.CaptionTextStyle.copyWith(
                            color: style.TextColorDark),
                      ))),
                  Expanded(
                      flex: 2,
                      child: Center(
                          child: Text(
                        'Rp 0',
                        style: style.CaptionTextStyle,
                      ))),
                ],
              )),
          SizedBox(height: 12),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Total Iuran',
                    style: style.Display2TextStyle.copyWith(
                        color: style.TextColorDark)),
                Text(
                  'Rp 10.000.000',
                  style: style.Display1TextStyle.copyWith(
                      color: style.primaryColor),
                )
              ],
            ),
          )
        ]),
      ),
    );
  }
}
