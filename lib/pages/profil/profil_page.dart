import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:image_picker/image_picker.dart';
import 'package:koperasi_jasa/pages/auth/login_page.dart';
import 'package:koperasi_jasa/pages/password/change_password_page.dart';
import 'package:koperasi_jasa/styles/style.dart' as style;
import 'package:shared_preferences/shared_preferences.dart';

class ProfilPage extends StatefulWidget {
  @override
  _ProfilPageState createState() => _ProfilPageState();
}

enum LoginStatus { notSignIn, signIn }

class _ProfilPageState extends State<ProfilPage> {
  LoginStatus _loginStatus = LoginStatus.signIn;
  String username, email, namaLengkap, namaIbu, noKtp, noHp, alamat;

  File _image;

  Future getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      username = preferences.getString("username");
      email = preferences.getString("email");
      namaLengkap = preferences.getString("namaLengkap");
      namaIbu = preferences.getString("namaIbu");
      noHp = preferences.getString("noHp");
      alamat = preferences.getString("alamat");
      noKtp = preferences.getString("noKtp");
    });
  }

  Future pickImage(ImageSource source) async {
    try {
      final pickedFile = await ImagePicker().getImage(source: source);

      setState(() {
        _image = File(pickedFile.path);
      });
    } catch (e) {
      print(e);
    }
  }

  void _chooseImageSource() {
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        // height: 146,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 12),
              child: Text('Pilih Sumber',
                  style: style.Display2TextStyle.copyWith(
                      color: style.TextColorDark)),
            ),
            ListTile(
              onTap: () {
                pickImage(ImageSource.camera);

                Navigator.pop(context);
              },
              leading: CircleAvatar(
                  child:
                      Icon(FlutterIcons.camera_fea, color: style.primaryColor),
                  backgroundColor: style.backgroundColor),
              title: Text('Camera'),
            ),
            ListTile(
              onTap: () {
                pickImage(ImageSource.gallery);
                Navigator.pop(context);
              },
              leading: CircleAvatar(
                  child:
                      Icon(FlutterIcons.folder_fea, color: style.primaryColor),
                  backgroundColor: style.backgroundColor),
              title: Text('Gallery'),
            )
          ],
        ),
      ),
    );

    showDialog(context: context, child: alertDialog);
  }

  signOut() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.clear();
      _loginStatus = LoginStatus.notSignIn;
    });
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: PreferredSize(
              child: Container(
                padding: const EdgeInsets.all(16),
                color: Colors.white,
                width: double.infinity,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    _image == null
                        ? Center(
                            child: Stack(
                              alignment: AlignmentDirectional.bottomCenter,
                              children: <Widget>[
                                Container(
                                  width: 72,
                                  height: 72,
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(16),
                                      child: Image.asset(
                                        'assets/images/logoKPTI.png',
                                        fit: BoxFit.cover,
                                      )),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    color: Colors.black12.withOpacity(0.3),
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(16),
                                        bottomRight: Radius.circular(16)),
                                  ),
                                  width: 72,
                                  height: 24,
                                  child: FlatButton.icon(
                                    onPressed: () {
                                      _chooseImageSource();
                                    },
                                    icon: Icon(
                                      FlutterIcons.edit_fea,
                                      color: Colors.white,
                                      size: 10,
                                    ),
                                    label: Text(
                                      'Edit',
                                      style: style.Body2TextStyle.copyWith(
                                          fontSize: 12, color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        : Center(
                            child: Stack(
                              alignment: AlignmentDirectional.bottomCenter,
                              children: <Widget>[
                                Container(
                                  width: 72,
                                  height: 72,
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(16),
                                      child: Image.file(
                                        _image,
                                        fit: BoxFit.cover,
                                      )),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    color: Colors.black12.withOpacity(0.3),
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(16),
                                        bottomRight: Radius.circular(16)),
                                  ),
                                  width: 72,
                                  height: 24,
                                  child: FlatButton.icon(
                                    onPressed: () {
                                      _chooseImageSource();
                                    },
                                    icon: Icon(
                                      FlutterIcons.edit_fea,
                                      color: Colors.white,
                                      size: 10,
                                    ),
                                    label: Text(
                                      'Edit',
                                      style: style.Body2TextStyle.copyWith(
                                          fontSize: 12, color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                    SizedBox(width: 16),
                    Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Selamat Datang,',
                          style: style.CaptionTextStyle,
                        ),
                        Text(
                          '$namaLengkap',
                          style: style.Display1TextStyle,
                        ),
                        Text(
                          '10231278247',
                          style: style.Display2TextStyle.copyWith(
                              color: style.primaryColor),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              preferredSize: Size.fromHeight(96)),
          body: ListView(
            padding: const EdgeInsets.only(bottom: 16),
            children: <Widget>[
              SizedBox(height: 8),
              Container(
                  color: Colors.white,
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Text(
                          'Data Diri',
                          style: style.Display1TextStyle,
                        ),
                      ),
                      Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Username',
                                style: style.Body3TextStyle.copyWith(
                                    fontSize: 14)),
                            Text(
                              '$username',
                              style: style.Body2TextStyle,
                            )
                          ]),
                      SizedBox(height: 12),
                      Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Nomor KTP',
                                style: style.Body3TextStyle.copyWith(
                                    fontSize: 14)),
                            Text(
                              '$noKtp',
                              style: style.Body2TextStyle,
                            )
                          ]),
                      SizedBox(height: 12),
                      Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Nama Ibu',
                                style: style.Body3TextStyle.copyWith(
                                    fontSize: 14)),
                            Text(
                              '$namaIbu',
                              style: style.Body2TextStyle,
                            )
                          ]),
                      SizedBox(height: 12),
                      Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Nomor HP',
                                style: style.Body3TextStyle.copyWith(
                                    fontSize: 14)),
                            Text(
                              '$noHp',
                              style: style.Body2TextStyle,
                            )
                          ]),
                      SizedBox(height: 12),
                      Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Email',
                                style: style.Body3TextStyle.copyWith(
                                    fontSize: 14)),
                            Text(
                              '$email',
                              style: style.Body2TextStyle,
                            )
                          ]),
                      SizedBox(height: 12),
                      Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Alamat',
                                style: style.Body3TextStyle.copyWith(
                                    fontSize: 14)),
                            Text(
                              '$alamat',
                              style: style.Body2TextStyle,
                            )
                          ]),
                    ],
                  )),
              SizedBox(height: 8),
              Ink(
                  color: Colors.white,
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChangePasswordPage()));
                    },
                    child: Container(
                        width: double.infinity,
                        height: 48,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 16.0),
                          child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(FlutterIcons.lock_fea,
                                    color: style.primaryColor, size: 20),
                                SizedBox(width: 12),
                                Text('Ganti Password',
                                    style: style.Display2TextStyle.copyWith(
                                        color: style.primaryColor)),
                              ]),
                        )),
                  )),
              SizedBox(height: 8),
              Ink(
                  color: Colors.white,
                  child: InkWell(
                    onTap: () {
                      signOut();
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (context) => LoginPage()),
                          (Route<dynamic> route) => false);
                    },
                    child: Container(
                        width: double.infinity,
                        height: 48,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 16.0),
                          child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(FlutterIcons.log_out_fea,
                                    color: style.redIcon, size: 20),
                                SizedBox(width: 12),
                                Text('Logout',
                                    style: style.Display2TextStyle.copyWith(
                                        color: style.redIcon)),
                              ]),
                        )),
                  )),
            ],
          )),
    );
  }
}
