import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:image_picker/image_picker.dart';
import 'package:koperasi_jasa/pages/home/home_page.dart';
import 'package:koperasi_jasa/pages/root/root_page.dart';
import 'package:koperasi_jasa/styles/style.dart' as style;

class NewPaymentPage extends StatefulWidget {
  @override
  _NewPaymentPageState createState() => _NewPaymentPageState();
}

class _NewPaymentPageState extends State<NewPaymentPage> {
  File _image;

  Future pickImage(ImageSource source) async {
    try {
      final pickedFile = await ImagePicker().getImage(source: source);

      setState(() {
        _image = File(pickedFile.path);
      });
    } catch (e) {
      print(e);
    }
  }

  void _chooseImageSource() {
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        // height: 146,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 12),
              child: Text('Pilih Sumber',
                  style: style.Display2TextStyle.copyWith(
                      color: style.TextColorDark)),
            ),
            ListTile(
              onTap: () {
                pickImage(ImageSource.camera);

                Navigator.pop(context);
              },
              leading: CircleAvatar(
                  child:
                      Icon(FlutterIcons.camera_fea, color: style.primaryColor),
                  backgroundColor: style.backgroundColor),
              title: Text('Camera'),
            ),
            ListTile(
              onTap: () {
                pickImage(ImageSource.gallery);
                Navigator.pop(context);
              },
              leading: CircleAvatar(
                  child:
                      Icon(FlutterIcons.folder_fea, color: style.primaryColor),
                  backgroundColor: style.backgroundColor),
              title: Text('Gallery'),
            )
          ],
        ),
      ),
    );

    showDialog(context: context, child: alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Iuran Baru'),
        ),
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: <Widget>[
            Card(
              color: Colors.white,
              elevation: 0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                Radius.circular(16),
              )),
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                      child: Text(
                        'Tagihan',
                        style: style.Body1TextStyle,
                      ),
                    ),
                    Ink(
                      decoration: BoxDecoration(
                          color: style.backgroundColor,
                          borderRadius: BorderRadius.circular(16)),
                      child: ListTile(
                        leading: Container(
                            height: 42,
                            width: 42,
                            decoration: BoxDecoration(
                              border:
                                  Border.all(color: style.redIcon, width: 1.5),
                              borderRadius: BorderRadius.circular(21),
                            ),
                            child: Icon(
                              FlutterIcons.dollar_sign_fea,
                              size: 18,
                              color: style.redIcon,
                            )),
                        title: Text('Iuran Wajib Juni',
                            style: style.Body3TextStyle.copyWith(fontSize: 14)),
                        subtitle:
                            Text('Rp 10.000.000', style: style.Body2TextStyle),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16.0, bottom: 8),
                      child: Text(
                        'Iuran Lain',
                        style: style.Body1TextStyle,
                      ),
                    ),
                    Form(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          TextFormField(
                            textCapitalization: TextCapitalization.words,
                            decoration: InputDecoration(
                              filled: true,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(18),
                                  gapPadding: 0),
                              labelText: 'Iuran Pokok',
                              hintText: 'Masukkan Iuran Pokok HP',
                            ),
                            keyboardType: TextInputType.number,
                          ),
                          SizedBox(height: 8),
                          TextFormField(
                            textCapitalization: TextCapitalization.words,
                            decoration: InputDecoration(
                              filled: true,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(18),
                                  gapPadding: 0),
                              labelText: 'Iuran Sukarela',
                              hintText: 'Masukkan Iuran Sukarela',
                            ),
                            keyboardType: TextInputType.number,
                          ),
                          SizedBox(height: 8),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 16.0, bottom: 8),
                            child: Text(
                              'Bukti Pembayaran',
                              style: style.Body1TextStyle,
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            // color: Colors.white,
                            // padding: const EdgeInsets.only(top: 28, bottom: 12),
                            child: Center(
                                child: _image == null
                                    ? Container(
                                        padding: const EdgeInsets.all(12),
                                        width: double.infinity,
                                        decoration: BoxDecoration(
                                            color: style.backgroundColor,
                                            borderRadius:
                                                BorderRadius.circular(16)),
                                        child: Center(
                                          child: InkWell(
                                            onTap: () {
                                              _chooseImageSource();
                                            },
                                            child: Column(
                                              mainAxisSize: MainAxisSize.max,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(
                                                  FlutterIcons.plus_square_fea,
                                                  size: 48.0,
                                                  color: style.TextColor1,
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8),
                                                  child: Text(
                                                      'Upload Bukti Pembayaran',
                                                      style: style
                                                              .Body2TextStyle
                                                          .copyWith(
                                                              color: style
                                                                  .TextColor1)),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      )
                                    : Center(
                                        child: Stack(
                                          alignment:
                                              AlignmentDirectional.topEnd,
                                          children: <Widget>[
                                            Container(
                                              // height: 120,
                                              width: double.infinity,
                                              child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(16),
                                                  child: Image.file(
                                                    _image,
                                                    fit: BoxFit.cover,
                                                  )),
                                            ),
                                            Container(
                                              decoration: BoxDecoration(
                                                color: Colors.black12
                                                    .withOpacity(0.3),
                                                borderRadius:
                                                    BorderRadius.circular(16),
                                              ),
                                              height: 40,
                                              width: 120,
                                              child: FlatButton.icon(
                                                onPressed: () {
                                                  _chooseImageSource();
                                                },
                                                icon: Icon(
                                                  FlutterIcons.edit_fea,
                                                  color: Colors.white,
                                                  size: 10,
                                                ),
                                                label: Text(
                                                  'Edit Gambar',
                                                  style: style.Body2TextStyle
                                                      .copyWith(
                                                          fontSize: 12,
                                                          color: Colors.white),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 24),
                    Container(
                      width: double.infinity,
                      height: 48,
                      child: FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16)),
                        onPressed: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => RootPage()));
                        },
                        child: Text(
                          'Kirim Sekarang',
                          style: style.ButtonTextStyle,
                        ),
                        color: style.primaryColor,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
