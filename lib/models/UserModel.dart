class UserModel {
  final String id;
  final String username;
  final String namaKtp;
  final String namaIbu;
  final String noHp;
  final String alamat;
  final String fotoKtp;
  final String fotoSelfieKtp;
  final String isVerified;

  UserModel(this.id, this.username, this.namaKtp, this.namaIbu, this.noHp,
      this.alamat, this.fotoKtp, this.fotoSelfieKtp, this.isVerified);
}
