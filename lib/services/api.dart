class ApiUrl {
  static String baseUrl = "kpti.co.id/api-koperasi";

  static String login = "https://$baseUrl/login.php";
  static String getUser = "https://$baseUrl/getprofile.php?id_user=";
  static String register = "https://$baseUrl/register-anggota.php";
}
